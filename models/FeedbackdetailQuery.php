<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Feedbackdetail]].
 *
 * @see Feedbackdetail
 */
class FeedbackdetailQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Feedbackdetail[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Feedbackdetail|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
