<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Customer;
use DateTime;

/**
 * CustomerSearch represents the model behind the search form about `app\models\Customer`.
 */
class CustomerSearch extends Customer
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'contact_no', 'sms_promotion', 'link_to_facebook', 'status', 'outlet_id'], 'integer'],
            [['name', 'birthdate', 'city', 'email', 'facebook_link', 'created_on', 'updated_on', 'created_by', 'updated_by'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Customer::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if(Yii::$app->user->identity->outlet_id==1){
            $query->andFilterWhere([
                'outlet_id' => $this->outlet_id,
            ]);
        }
        else{
            $query->andFilterWhere([
                'outlet_id' => Yii::$app->user->identity->outlet_id,
            ]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'contact_no' => $this->contact_no,
            'sms_promotion' => $this->sms_promotion,
            'link_to_facebook' => $this->link_to_facebook,
            'status' => $this->status,
            //'outlet_id' => $this->outlet_id,
            'created_on' => $this->created_on,
            'created_by' => $this->created_by,
            'updated_on' => $this->updated_on,
            'updated_by' => $this->updated_by,
        ]);

        if($this->birthdate)
        {
            $mysql_date = $this->birthdate;
            // date in Y-m-d format as MySQL stores it
            $date_obj = date_create_from_format('d-m-Y',$mysql_date);
            $created_on = date_format($date_obj, 'Y-m-d');
            $query->andFilterWhere(['like', 'birthdate', $created_on]);
        }

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'facebook_link', $this->facebook_link]);

        return $dataProvider;
    }

    public function report($params)
    {

        if($params['CustomerSearch']['created_on'])
        {
            $query = Customer::find()->joinWith('feedbacks');

        }else {
            $start_date = date('Y-m-d');
            $query = Customer::find()->joinWith('feedbacks')->andWhere(['like', 'customer.updated_on', $start_date]);
        }

       // $query = Customer::find()->joinWith('feedbacks');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        if ( ! is_null($this->created_on) && strpos($this->created_on, ' - ') !== false ) {
            list($start_date, $end_date) = explode(' - ', $this->created_on);
            $start_date = DateTime::createFromFormat('d/m/Y', $start_date);
            $start_date = $start_date->format('Y-m-d');
            $start_date = $start_date.' 00:00:00';
            $end_date = DateTime::createFromFormat('d/m/Y', $end_date);
            $end_date = $end_date->format('Y-m-d');
            $end_date = $end_date.' 23:59:59';
            $query->andFilterWhere(['between', 'customer.updated_on', $start_date, $end_date]);
        }

        $query->andFilterWhere([
            'customer.outlet_id' => $this->outlet_id,
        ]);


        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            // 'outlet_id' => $this->outlet_id,

            //'created_on' => $this->created_on,
            'created_by' => $this->created_by,
            'updated_on' => $this->updated_on,
            'updated_by' => $this->updated_by,
        ]);
        return $dataProvider;

    }
}
