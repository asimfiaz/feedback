<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "emoji".
 *
 * @property integer $emoji_id
 * @property string $emoji_name
 * @property string $emoji_content
 */
class Emoji extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'emoji';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['emoji_content'], 'file'],
            [['emoji_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'emoji_id' => 'Emoji ID',
            'emoji_name' => 'Emoji Name',
            'emoji_content' => 'Emoji Content',
        ];
    }
}
