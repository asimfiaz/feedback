<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "feedbackdetail".
 *
 * @property integer $id
 * @property integer $feedback_id
 * @property integer $categoryid
 * @property integer $subcategoryid
 * @property integer $reviewtypeid
 *
 * @property Feedback $feedback
 * @property Category $category
 * @property Subcategory $subcategory
 * @property Reviewtype $reviewtype
 */
class Feedbackdetail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'feedbackdetail';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['feedback_id', 'categoryid', 'subcategoryid', 'reviewtypeid'], 'required'],
            [['feedback_id', 'categoryid', 'subcategoryid', 'reviewtypeid'], 'integer'],
            [['feedback_id'], 'exist', 'skipOnError' => true, 'targetClass' => Feedback::className(), 'targetAttribute' => ['feedback_id' => 'id']],
            [['categoryid'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['categoryid' => 'id']],
            [['subcategoryid'], 'exist', 'skipOnError' => true, 'targetClass' => Subcategory::className(), 'targetAttribute' => ['subcategoryid' => 'id']],
            [['reviewtypeid'], 'exist', 'skipOnError' => true, 'targetClass' => Reviewtype::className(), 'targetAttribute' => ['reviewtypeid' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'feedback_id' => Yii::t('app', 'Feedback ID'),
            'categoryid' => Yii::t('app', 'Categoryid'),
            'subcategoryid' => Yii::t('app', 'Subcategoryid'),
            'reviewtypeid' => Yii::t('app', 'Reviewtypeid'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeedback()
    {
        return $this->hasOne(Feedback::className(), ['id' => 'feedback_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'categoryid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubcategory()
    {
        return $this->hasOne(Subcategory::className(), ['id' => 'subcategoryid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReviewtype()
    {
        return $this->hasOne(Reviewtype::className(), ['id' => 'reviewtypeid']);
    }

    /**
     * @inheritdoc
     * @return FeedbackdetailQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FeedbackdetailQuery(get_called_class());
    }
}
