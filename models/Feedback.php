<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "feedback".
 *
 * @property integer $id
 * @property integer $outlet_id
 * @property integer $customer_id
 * @property integer $sms_promo
 * @property string $overall_rating
 * @property string $remarks
 * @property string $created_on
 * @property integer $created_by
 * @property string $updated_on
 * @property integer $updated_by
 *
 * @property Outlet $outlet
 * @property Customer $customer
 */
class Feedback extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'feedback';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['outlet_id', 'customer_id', 'created_by', 'updated_by','sms_promo'], 'integer'],
            [['created_on', 'updated_on'], 'safe'],
            [['overall_rating'], 'string', 'max' => 4],
            [['remarks'], 'string', 'max' => 255],
            [['outlet_id'], 'exist', 'skipOnError' => true, 'targetClass' => Outlet::className(), 'targetAttribute' => ['outlet_id' => 'id']],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['customer_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'outlet_id' => Yii::t('app', 'Outlet Name'),
            'customer_id' => Yii::t('app', 'Customer Name'),
            'sms_promo'=>Yii::t('app', 'SMS Promo'),
            'overall_rating' => Yii::t('app', 'Overall Rating'),
            'remarks' => Yii::t('app', 'Remarks'),
            'created_on' => Yii::t('app', 'Feedback Date'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_on' => Yii::t('app', 'Updated On'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOutlet()
    {
        return $this->hasOne(Outlet::className(), ['id' => 'outlet_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }

    public function getFeedBackDetail()
    {
        return $this->hasMany(Feedbackdetail::className(), ['feedback_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return FeedbackQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FeedbackQuery(get_called_class());
    }
}
