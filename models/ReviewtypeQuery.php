<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Reviewtype]].
 *
 * @see Reviewtype
 */
class ReviewtypeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Reviewtype[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Reviewtype|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
