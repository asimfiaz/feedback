<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Emoji;

/**
 * EmojiSearch represents the model behind the search form about `app\models\Emoji`.
 */
class EmojiSearch extends Emoji
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['emoji_id'], 'integer'],
            [['emoji_name', 'emoji_content'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Emoji::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'emoji_id' => $this->emoji_id,
        ]);

        $query->andFilterWhere(['like', 'emoji_name', $this->emoji_name])
            ->andFilterWhere(['like', 'emoji_content', $this->emoji_content]);

        return $dataProvider;
    }
}
