<?php

namespace app\controllers;

use app\models\EmojiSearch;
use Yii;
use app\models\Reviewtype;
use app\models\ReviewtypeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\controllers\Html;

/**
 * ReviewtypeController implements the CRUD actions for Reviewtype model.
 */
class ReviewtypeController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Reviewtype models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ReviewtypeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionImage($id) {
        $emoji = EmojiSearch::find()->where('emoji_id='.$id)->one();
        return $emoji->emoji_content;
    }

    public function getEmojiImage($id) {
        $emoji = EmojiSearch::find()->where('emoji_id='.$id)->one();
        return $emoji->emoji_content;
    }

    public function getEmojis() {
        $emojis = EmojiSearch::find()->all();

        $returning = array();
        foreach ($emojis as $emoji) {
            //$img = "<img src='".Yii::$app->homeUrl.'frontend/'.$emoji['emoji_content']."' style='width: 50px;' />";

            $returning[$emoji['emoji_id']] = $emoji['emoji_name'];
                
        }
        
        return $returning;
    }

    /**
     * Displays a single Reviewtype model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Reviewtype model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Reviewtype();

        if ($model->load(Yii::$app->request->post())) {
            //echo '<pre>'; print_r($model); echo '</pre>'; exit();

            if (empty($model->status)) {
                $model->status = 1;
            }

            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Reviewtype model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Reviewtype model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Reviewtype model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Reviewtype the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Reviewtype::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
