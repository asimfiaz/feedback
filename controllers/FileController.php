<?php

namespace app\controllers;

use Yii;
use app\models\File;
use app\models\FileSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * FileController implements the CRUD actions for File model.
 */
class FileController extends Controller {
  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
    ];
  }

  /**
   * Lists all File models.
   * @return mixed
   */
  public function actionIndex() {
    $searchModel = new FileSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Displays a single File model.
   * @param integer $id
   * @return mixed
   */
  public function actionView($id) {
    return $this->render('view', [
      'model' => $this->findModel($id),
    ]);
  }

  /**
   * Creates a new File model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionCreate() {
    $model = new File();

    // !!! if the request is NOT a POST
    if (!$model->load(Yii::$app->request->post())) {
      return $this->render('create', [
        'model' => $model,
      ]);
    }

    $model->name = UploadedFile::getInstance($model, 'name');
    
    //echo '<pre>'; print_r($model); echo '</pre>'; exit();

    if (!empty($model->name)) {
      $model->name->saveAs(
        'frontend/elisyam/img/feedback_background_music.' . 
        $model->name->extension
      );

      $model->name = 'feedback_background_music.' . $model->name->extension;
      
      if ($model->save()) {
        return $this->redirect(['index', 'msg' => 1]);
      }
    }
    
    return $this->render('create', [
      'model' => $model,
    ]);
  }

  /**
   * Updates an existing File model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   */
  public function actionUpdate($id) {
    $model = $this->findModel($id);

    // !!! If request is NOT a POST
    if (!$model->load(Yii::$app->request->post())) {
      return $this->render('create', [
        'model' => $model,
      ]);
    }

    $file = UploadedFile::getInstance($model, 'name');

    // Only update the file if a new one is selected.
    if (!empty($file)) {
      // Check if file is mp3/mp4 and smaller than 10MB
      if (!$this->checkformat($file->extension) || !$this->checkSize($file->size)) {
        return $this->redirect(['index', 'msg' => 2]);
      }

      if ($file->saveAs(
          'frontend/elisyam/img/feedback_background_music.' . $file->extension)) {
        $model->name = 'feedback_background_music.' . $file->extension;
      }
    } 

    if ($model->save()) {
      return $this->redirect(['index', 'msg' => 1]);
    }

    return $file->error;
  }

  /**
   * Check if size of a file is below a limit.
   * 
   * @param int $file The size of a file (Bytes)
   * @param int $file_size_limit The size limit (MB) Default limit is 10 MBs.
   * @return bool True if file size is smaller than limit.
   */
  public function checkSize($file, $file_size_limit = 10) {
    return $file < ($file_size_limit * 1024 * 1024) ? true : false;
  }

  /**
   * Check if a file extension is mp3 or mp4.
   * 
   * @param string $file The file extension.
   * @return bool False if extension is not mp3 or mp4.
   */
  public function checkformat($file) {
    return $file == 'mp3' || $file == 'mp4' ? true : false;
  }

  /**
   * Deletes an existing File model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   */
  public function actionDelete($id) {
    $this->findModel($id)->delete();
    return $this->redirect(['index']);
  }


  /**
   * Finds the File model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return File the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */

  protected function findModel($id) {
    if (($model = File::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }
}
