<?php

namespace app\controllers;

use app\helpers\Helper;
use Yii;
use app\models\Emoji;
use app\models\EmojiSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * EmojiController implements the CRUD actions for Emoji model.
 */
class EmojiController extends Controller
{
  /**
   * @inheritdoc
   */
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
    ];
  }

  /**
   * Lists all Emoji models.
   * @return mixed
   */
  public function actionIndex()
  {
    $searchModel = new EmojiSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Displays a single Emoji model.
   * @param integer $id
   * @return mixed
   */
  public function actionView($id)
  {
    return $this->render('view', [
      'model' => $this->findModel($id),
    ]);
  }

  /**
   * Creates a new Emoji model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionCreate() {
    $model = new Emoji();

    if ($model->load(Yii::$app->request->post())) {
      //echo "<pre>"; print_r($name); echo "</pre>"; exit();

      $model->emoji_content = UploadedFile::getInstance($model, 'emoji_content');

      if ($model->emoji_content) {     
        // Generating a random name for image so replacing images
        // with same name doesn't happen.
        $name = Helper::generateImageName();

        $model->emoji_content->saveAs(
          'frontend/'
          .$name
          .'.'
          .$model->emoji_content->extension
        );

        $model->emoji_content = $name.'.'.$model->emoji_content->extension;


        if ($model->save()) {
          return $this->redirect(['view', 'id' => $model->emoji_id]);
        }
      }
    }

    return $this->render('create', [
      'model' => $model,
    ]);
  }

  /**
   * Updates an existing Emoji model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   */
  public function actionUpdate($id) {
    $model = $this->findModel($id);
    $old_emoji = $model->emoji_content;

    if ($model->load(Yii::$app->request->post())) {
      $model->emoji_content = UploadedFile::getInstance($model, 'emoji_content');

      if ($model->emoji_content) {     
        // Generating a random name for image so replacing images
        // with same name doesn't happen.
        $name = Helper::generateImageName();
           
        $model->emoji_content->saveAs(
          'frontend/'
          .$name
          .'.'
          .$model->emoji_content->extension
        );

        $model->emoji_content = $name.'.'.$model->emoji_content->extension;
      } else {
        $model->emoji_content = $old_emoji;
      }

      if ($model->save()) {
          return $this->redirect(['view', 'id' => $model->emoji_id]);
        }
    }

    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      return $this->redirect(['view', 'id' => $model->emoji_id]);
    } else {
      return $this->render('update', [
        'model' => $model,
      ]);
    }
  }

  /**
   * Deletes an existing Emoji model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   */
  public function actionDelete($id) {
    $this->findModel($id)->delete();

    return $this->redirect(['index']);
  }

  /**
   * Finds the Emoji model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return Emoji the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id) {
    if (($model = Emoji::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }
}
