<?php

namespace app\controllers;

use app\models\Customer;
use app\models\Feedback;
use app\models\Feedbackdetail;
use DateTime;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\helpers\Helper;
use app\models\Category;
use app\models\Reviewtype;
use app\models\Emoji;
use yii\db\cubrid\QueryBuilder;
use yii\db\Query;

class SiteController extends Controller
{
  /**
   * @inheritdoc
   */
  /*
  public function behaviors() {
    return [
        'access' => [
            'class' => AccessControl::className(),
            'only' => ['logout'],
            'rules' => [
                [
                    'actions' => ['logout'],
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ],
        'verbs' => [
            'class' => VerbFilter::className(),
            'actions' => [
                'logout' => ['post'],
            ],
        ],
    ];
  }*/

  public function behaviors() {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'rules' => [
          [
            'actions' => ['login', 'error'],
            'allow' => true,
          ],
          [
            'actions' => ['logout', 'index', 'getcustdetail', 'thankyou', 'thak', 'home', 'chart-report'], // add all actions to take guest to login page
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ],
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'logout' => ['get'],
        ],
      ],
    ];
  }

  /**
   * @inheritdoc
   */
  public function actions() {
    return [
      'error' => [
        'class' => 'yii\web\ErrorAction',
      ],
      'captcha' => [
        'class' => 'yii\captcha\CaptchaAction',
        'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
      ],
    ];
  }

  /**
   * Displays homepage.
   *
   * @return string
   */
  public function actionIndex() {
    $cat = Category::find()
    ->where('status=1')
    ->all();

    $connection = Yii::$app->getDb();
    $command = $connection->createCommand(
      "SELECT * FROM reviewtype 
      JOIN emoji
      ON reviewtype.emoji_id = emoji.emoji_id
      WHERE status = 1"
    );
    $revtype = $command->queryAll();
    
    $emoji = Emoji::find()->all();
    
    $this->layout = 'feedback';

    return $this->render('index', [
      'cat' => $cat,
      'revtype' => $revtype,
      'emoji' => $emoji
    ]);
  }

  public function actionGetcustdetail() {
    $html = '';

    $model = Customer::find()
    ->where('contact_no = "' . $_POST['num'] . '"')
    ->one();

    echo '<pre>';
    echo print_r($model);
    echo '<pre>';
    //echo json_encode(array($model));
    //echo '34344';
    //echo json_encode(array('status'=>0));
  }

  public function actionGetcustomers() {
    $html = '';

    if (isset($_POST['id']) && !empty($_POST['id'])) {
      $model = Customer::find()
      ->where('outlet_id = ' . $_POST['id'])
      ->all();
      
      $html .= '<option>Select Customer</option>';
      foreach ($model as $v) {
        $html .= '<option value="' . $v->id . '">' . $v->name . '</option>';
      }

      echo json_encode(array('status' => 1, 'html' => $html));
    } else {
      $model = Customer::find()
      ->all();

      $html .= '<option>Select Customer</option>';
      foreach ($model as $v) {
        $html .= '<option value="' . $v->id . '">' . $v->name . '</option>';
      }

      echo json_encode(array('status' => 1, 'html' => $html));
    }
  }

  /**
   * Login action.
   *
   * @return string
   */
  public function actionLogin() {
    $this->layout = 'login';

    if (!Yii::$app->user->isGuest) {
      return $this->goHome();
    }

    $model = new LoginForm();

    if ($model->load(Yii::$app->request->post()) && $model->login()) {
      if (\Yii::$app->user->identity->rights == 'admin') {
        return $this->redirect(array('feedback/index'));
      } else {
        //return $this->goBack();
        return $this->redirect('home');
      }
    }

    return $this->render('login', [
      'model' => $model,
    ]);
  }

  /**
   * Logout action.
   *
   * @return string
   */
  public function actionLogout() {
    Yii::$app->user->logout();
    return $this->goHome();
  }

  /**
   * Displays contact page
   *
   * @return string
   */
  public function actionContact() {
    $model = new ContactForm();

    if ($model->load(Yii::$app->request->post()) && 
        $model->contact(Yii::$app->params['adminEmail'])) {
      Yii::$app->session->setFlash('contactFormSubmitted');

      return $this->refresh();
    }

    return $this->render('contact', [
      'model' => $model,
    ]);
  }

  public function actionThak() {
    $this->layout = 'feedback';
    //return $this->render('about');
    return $this->render('thankyou');
  }

  public function actionHome() {
    $this->layout = 'home';
    return $this->render('home');
  }

  public function actionThankyou() {
    /*echo "<pre>";
      print_r($_POST);
    echo "</pre>";exit;*/

    $getalready = Customer::find()
    ->where('contact_no =' . $_POST['phone'])
    ->one();

    if ($getalready) {
      $customer = $getalready;

      $getalready->updated_on = date("Y-m-d H:i:s");
      $getalready->name = $_POST['name'];
      $getalready->outlet_id = $_POST['outlet'];

      if (!empty($_POST['email'])) {
        $getalready->email = $_POST['email'];
      }
      
      if ($_POST['dob'] == '') {
        $getalready->birthdate = $getalready->birthdate;
      } else {
        $getalready->birthdate = DateTime::createFromFormat(
          'd-m-Y', 
          $_POST['dob']
        )->format('Y-m-d');
      }

      $getalready->save();
    } else {
      $customer = new Customer();

      $customer->name = $_POST['name'];
      $customer->outlet_id = $_POST['outlet'];
      $customer->email = $_POST['email'];
      $customer->contact_no = $_POST['phone'];
      $customer->created_on = date("Y-m-d H:i:s");
      $customer->updated_on = date("Y-m-d H:i:s");
      $customer->created_by =  Yii::$app->user->id;

      if ($_POST['dob'] == '') {
        $customer->birthdate = null;
      } else {
        $customer->birthdate = DateTime::createFromFormat('d-m-Y', $_POST['dob'])->format('Y-m-d');
      }
      
      $customer->save();
    }

    $feedback = new Feedback();

    $feedback->customer_id = $customer->id;
    $feedback->outlet_id = $_POST['outlet'];
    $feedback->overall_rating = $_POST['myRate1'];
    $feedback->remarks = $_POST['remarks'];
    $feedback->created_on = date("Y-m-d H:i:s");
    $feedback->created_by =  Yii::$app->user->id;

    if ($_POST['switch'] == 'on') {
      $feedback->sms_promo = 1;
    } else {
      $feedback->sms_promo = 0;
    }

    if (!$feedback->save()) {
      echo '<pre>';
      echo print_r($feedback);
      echo '<pre>';
      exit;
    }

    foreach ($_POST['subcatradio'] as $key => $value) {
      //$value = $_POST['subcatradio'];
      $feedbackDetail = new Feedbackdetail();

      $val = explode("-", $value);
      $feedbackDetail->feedback_id = $feedback->id;
      $feedbackDetail->categoryid = $val[0];
      $feedbackDetail->subcategoryid = $val[1];
      $feedbackDetail->reviewtypeid = $val[2];

      if (!$feedbackDetail->save()) {
        echo '<pre>';
        echo print_r($feedbackDetail);
        echo '<pre>';
        exit;
      }
    }

    // $outlet = \app\models\Outlet::find()->where('id='.$_POST['outlet'])->one();
    // $customer->outlet_id

    $name = $_POST['name'];
    $phone = $_POST['phone'];
    $contact = $phone * 1;
    $contact = '92' . $contact;

    if (!empty($phone)) {
      $text = \app\models\SmsText::find()->one()->text;
      //$text = "Dear Customerhank you for visiting Al-khan restaurant and giving us your;
      $sendsms = Helper::sendSMS($contact, $text);
    }

    $this->layout = 'feedback';
    return $this->render('thankyou');
  }

  public  function actionChartReport() {
    return $this->render('chartreport');
  }
}
