-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 08, 2020 at 12:21 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `feedback_tab`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth_assignment`
--

CREATE TABLE IF NOT EXISTS `auth_assignment` (
  `item_name` varchar(64) NOT NULL,
  `user_id` varchar(64) NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_item`
--

CREATE TABLE IF NOT EXISTS `auth_item` (
  `name` varchar(64) NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text,
  `rule_name` varchar(64) DEFAULT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_item_child`
--

CREATE TABLE IF NOT EXISTS `auth_item_child` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_rule`
--

CREATE TABLE IF NOT EXISTS `auth_rule` (
  `name` varchar(64) NOT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
`id` int(11) NOT NULL,
  `cat_name` varchar(100) NOT NULL,
  `detail` varchar(100) DEFAULT NULL,
  `status` int(1) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `cat_name`, `detail`, `status`) VALUES
(1, 'FOOD', 'Food items ', 1),
(2, 'STAFF', 'STAFF Services', 1),
(3, 'Outlet', 'Outlet visited\r\n\r\n\r\n\r\n', 1);

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
`id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `contact_no` varchar(111) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `sms_promotion` int(1) DEFAULT NULL,
  `link_to_facebook` int(1) DEFAULT NULL,
  `facebook_link` varchar(100) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `outlet_id` int(11) NOT NULL,
  `created_on` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `name`, `contact_no`, `birthdate`, `city`, `email`, `sms_promotion`, `link_to_facebook`, `facebook_link`, `status`, `outlet_id`, `created_on`, `created_by`, `updated_on`, `updated_by`) VALUES
(33, 'asd', '12312312312', '2020-03-03', NULL, NULL, NULL, NULL, NULL, 1, 4, '2020-03-30 22:13:40', 3, '2020-03-30 22:13:40', NULL),
(34, 'asdasd', '12345678900', '2020-03-03', NULL, NULL, NULL, NULL, NULL, 1, 4, '2020-03-30 23:04:37', 3, '2020-03-31 15:24:22', NULL),
(35, 'Asim Fiaz', '03481181893', '2020-04-28', NULL, 'aaa@asd.com', NULL, NULL, NULL, 1, 4, '2020-04-02 11:04:44', 3, '2020-04-08 09:51:40', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `emoji`
--

CREATE TABLE IF NOT EXISTS `emoji` (
`emoji_id` int(11) NOT NULL,
  `emoji_name` varchar(255) DEFAULT NULL,
  `emoji_content` text
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `emoji`
--

INSERT INTO `emoji` (`emoji_id`, `emoji_name`, `emoji_content`) VALUES
(1, 'Excellent', 'excellent.png'),
(2, 'Good', 'good.png'),
(3, 'Fair', 'fair.png'),
(8, 'asd', 'beef_boneless_prime_cube.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE IF NOT EXISTS `feedback` (
`id` int(11) NOT NULL,
  `outlet_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `overall_rating` varchar(4) NOT NULL DEFAULT '0',
  `sms_promo` int(11) DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`id`, `outlet_id`, `customer_id`, `overall_rating`, `sms_promo`, `remarks`, `created_on`, `created_by`, `updated_on`, `updated_by`) VALUES
(31, 4, 33, '3', 0, 'asdasdasdasdas', '2020-03-30 22:13:41', 3, NULL, NULL),
(32, 4, 34, '5', 0, 'asdasdasddsdaasdasdasasd', '2020-03-30 23:04:37', 3, NULL, NULL),
(33, 4, 34, '4', 0, 'asd asd asd ', '2020-03-31 15:24:22', 3, NULL, NULL),
(34, 4, 35, '', 0, 'nais', '2020-04-02 11:04:45', 3, NULL, NULL),
(35, 4, 35, '5', 0, 'sdfg', '2020-04-02 11:08:24', 3, NULL, NULL),
(36, 4, 35, '2', 0, 'fgh', '2020-04-02 11:08:53', 3, NULL, NULL),
(37, 4, 35, '4', 0, 'fgh', '2020-04-02 11:14:52', 3, NULL, NULL),
(38, 4, 35, '3', 0, 'asd', '2020-04-02 11:23:18', 3, NULL, NULL),
(39, 4, 35, '4', 0, 'zxcnmzdfhkjshdfsdf', '2020-04-02 11:34:50', 3, NULL, NULL),
(40, 4, 35, '4', 0, 'something', '2020-04-07 10:48:15', 3, NULL, NULL),
(41, 4, 35, '4', 0, 'something', '2020-04-07 13:33:38', 3, NULL, NULL),
(42, 4, 35, '4', 0, 'aasdasd', '2020-04-07 14:37:45', 3, NULL, NULL),
(43, 4, 35, '4', 0, 'beef test', '2020-04-07 17:49:11', 3, NULL, NULL),
(44, 4, 35, '5', 0, 'smilies test', '2020-04-08 09:51:40', 3, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `feedbackdetail`
--

CREATE TABLE IF NOT EXISTS `feedbackdetail` (
`id` int(11) NOT NULL,
  `feedback_id` int(11) NOT NULL,
  `categoryid` int(11) NOT NULL,
  `subcategoryid` int(11) NOT NULL,
  `reviewtypeid` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=472 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feedbackdetail`
--

INSERT INTO `feedbackdetail` (`id`, `feedback_id`, `categoryid`, `subcategoryid`, `reviewtypeid`) VALUES
(391, 31, 1, 1, 1),
(392, 32, 1, 1, 2),
(393, 33, 1, 1, 2),
(394, 39, 1, 1, 1),
(395, 39, 1, 2, 1),
(396, 39, 1, 3, 1),
(397, 39, 1, 4, 1),
(398, 39, 1, 5, 1),
(399, 39, 2, 6, 1),
(400, 39, 2, 7, 1),
(401, 39, 2, 8, 1),
(402, 39, 2, 9, 1),
(403, 39, 3, 10, 1),
(404, 39, 3, 11, 1),
(405, 39, 3, 12, 1),
(406, 39, 3, 13, 1),
(407, 40, 1, 1, 1),
(408, 40, 1, 2, 1),
(409, 40, 1, 3, 1),
(410, 40, 1, 4, 1),
(411, 40, 1, 5, 1),
(412, 40, 2, 6, 1),
(413, 40, 2, 7, 1),
(414, 40, 2, 8, 1),
(415, 40, 2, 9, 1),
(416, 40, 3, 10, 2),
(417, 40, 3, 11, 2),
(418, 40, 3, 12, 2),
(419, 40, 3, 13, 2),
(420, 41, 1, 1, 1),
(421, 41, 1, 2, 1),
(422, 41, 1, 3, 1),
(423, 41, 1, 4, 1),
(424, 41, 1, 5, 1),
(425, 41, 2, 6, 1),
(426, 41, 2, 7, 1),
(427, 41, 2, 8, 1),
(428, 41, 2, 9, 1),
(429, 41, 3, 10, 2),
(430, 41, 3, 11, 2),
(431, 41, 3, 12, 2),
(432, 41, 3, 13, 2),
(433, 42, 1, 1, 1),
(434, 42, 1, 2, 1),
(435, 42, 1, 3, 1),
(436, 42, 1, 4, 1),
(437, 42, 1, 5, 1),
(438, 42, 2, 6, 1),
(439, 42, 2, 7, 1),
(440, 42, 2, 8, 1),
(441, 42, 2, 9, 1),
(442, 42, 3, 10, 1),
(443, 42, 3, 11, 1),
(444, 42, 3, 12, 1),
(445, 42, 3, 13, 1),
(446, 43, 1, 1, 4),
(447, 43, 1, 2, 4),
(448, 43, 1, 3, 4),
(449, 43, 1, 4, 4),
(450, 43, 1, 5, 4),
(451, 43, 2, 6, 4),
(452, 43, 2, 7, 4),
(453, 43, 2, 8, 4),
(454, 43, 2, 9, 4),
(455, 43, 3, 10, 4),
(456, 43, 3, 11, 4),
(457, 43, 3, 12, 4),
(458, 43, 3, 13, 4),
(459, 44, 1, 1, 2),
(460, 44, 1, 2, 2),
(461, 44, 1, 3, 2),
(462, 44, 1, 4, 2),
(463, 44, 1, 5, 2),
(464, 44, 2, 6, 2),
(465, 44, 2, 7, 2),
(466, 44, 2, 8, 2),
(467, 44, 2, 9, 2),
(468, 44, 3, 10, 2),
(469, 44, 3, 11, 2),
(470, 44, 3, 12, 2),
(471, 44, 3, 13, 2);

-- --------------------------------------------------------

--
-- Table structure for table `outlet`
--

CREATE TABLE IF NOT EXISTS `outlet` (
`id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `address` varchar(255) NOT NULL,
  `contactno` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `outlet`
--

INSERT INTO `outlet` (`id`, `name`, `address`, `contactno`, `email`, `status`) VALUES
(4, 'Alkhan Thoker Branch', ' Alkhan Thoker Branch Thokar Niaz Baig, Main Raiwind Road, Lahore', ' 0344-4449623-24-25', ' alkhan_3@hotmail.com', 1),
(5, 'Alkhan Shahdara Branch', '07-KM, Sheikhupura Road, Shahdara, Lahore, Pakistan', '923217912683', 'alkhan_3@hotmail.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `reviewtype`
--

CREATE TABLE IF NOT EXISTS `reviewtype` (
`id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `emoji_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reviewtype`
--

INSERT INTO `reviewtype` (`id`, `name`, `status`, `emoji_id`) VALUES
(1, 'Excellent', 1, 1),
(2, 'Good', 1, 2),
(3, 'Fair', 1, 3),
(4, 'Beef', 1, 8);

-- --------------------------------------------------------

--
-- Table structure for table `sms_text`
--

CREATE TABLE IF NOT EXISTS `sms_text` (
`id` int(11) NOT NULL,
  `text` varchar(10000) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sms_text`
--

INSERT INTO `sms_text` (`id`, `text`) VALUES
(2, '   Dear Customer!Thank you for MCE restaurant and giving us your valuable feedback.Contact# 051-2204040');

-- --------------------------------------------------------

--
-- Table structure for table `subcategory`
--

CREATE TABLE IF NOT EXISTS `subcategory` (
`id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `categoryid` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subcategory`
--

INSERT INTO `subcategory` (`id`, `name`, `categoryid`, `status`) VALUES
(1, 'Menu Variety', 1, 1),
(2, 'Presentation', 1, 1),
(3, 'Serving Size', 1, 1),
(4, 'Temperature', 1, 1),
(5, 'Taste', 1, 1),
(6, 'Appearance', 2, 1),
(7, 'Courtesy', 2, 1),
(8, 'Menu Knowledge', 2, 1),
(9, 'Speed of Service', 2, 1),
(10, 'Cleanliness', 3, 1),
(11, 'Menu Visibility', 3, 1),
(12, 'Value for Money', 3, 1),
(13, 'Overall Experience', 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `authKey` varchar(10) DEFAULT NULL,
  `user` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `rights` varchar(50) DEFAULT NULL,
  `outlet_id` int(11) DEFAULT NULL,
  `landing_page` varchar(1000) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_on` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `authKey`, `user`, `password`, `status`, `rights`, `outlet_id`, `landing_page`, `created_by`, `created_on`, `updated_by`, `updated_on`) VALUES
(2, 'Al Khan', 'mce@gmail.com', NULL, 'alkhan', 'alkhan1', 1, 'user', 4, NULL, NULL, NULL, NULL, NULL),
(3, 'admin', 'mce@maaliksoft.com', NULL, 'admin', 'admin', 1, 'admin', 4, NULL, NULL, '2019-02-07 00:00:00', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
 ADD PRIMARY KEY (`item_name`,`user_id`);

--
-- Indexes for table `auth_item`
--
ALTER TABLE `auth_item`
 ADD PRIMARY KEY (`name`), ADD KEY `rule_name` (`rule_name`), ADD KEY `type` (`type`);

--
-- Indexes for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
 ADD PRIMARY KEY (`parent`,`child`), ADD KEY `child` (`child`);

--
-- Indexes for table `auth_rule`
--
ALTER TABLE `auth_rule`
 ADD PRIMARY KEY (`name`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
 ADD PRIMARY KEY (`id`), ADD KEY `outlet_id` (`outlet_id`);

--
-- Indexes for table `emoji`
--
ALTER TABLE `emoji`
 ADD PRIMARY KEY (`emoji_id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
 ADD PRIMARY KEY (`id`), ADD KEY `outlet_id` (`outlet_id`), ADD KEY `customer_id` (`customer_id`);

--
-- Indexes for table `feedbackdetail`
--
ALTER TABLE `feedbackdetail`
 ADD PRIMARY KEY (`id`), ADD KEY `feedback_id` (`feedback_id`), ADD KEY `categoryid` (`categoryid`), ADD KEY `subcategoryid` (`subcategoryid`), ADD KEY `reviewtypeid` (`reviewtypeid`);

--
-- Indexes for table `outlet`
--
ALTER TABLE `outlet`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reviewtype`
--
ALTER TABLE `reviewtype`
 ADD PRIMARY KEY (`id`), ADD KEY `emoji_id` (`emoji_id`);

--
-- Indexes for table `sms_text`
--
ALTER TABLE `sms_text`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subcategory`
--
ALTER TABLE `subcategory`
 ADD PRIMARY KEY (`id`), ADD KEY `categoryid` (`categoryid`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD KEY `outlet_id` (`outlet_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `emoji`
--
ALTER TABLE `emoji`
MODIFY `emoji_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `feedbackdetail`
--
ALTER TABLE `feedbackdetail`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=472;
--
-- AUTO_INCREMENT for table `outlet`
--
ALTER TABLE `outlet`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `reviewtype`
--
ALTER TABLE `reviewtype`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `sms_text`
--
ALTER TABLE `sms_text`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `subcategory`
--
ALTER TABLE `subcategory`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `auth_item`
--
ALTER TABLE `auth_item`
ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `customer`
--
ALTER TABLE `customer`
ADD CONSTRAINT `customer_ibfk_1` FOREIGN KEY (`outlet_id`) REFERENCES `outlet` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `feedback`
--
ALTER TABLE `feedback`
ADD CONSTRAINT `feedback_ibfk_1` FOREIGN KEY (`outlet_id`) REFERENCES `outlet` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `feedback_ibfk_2` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `feedbackdetail`
--
ALTER TABLE `feedbackdetail`
ADD CONSTRAINT `feedbackdetail_ibfk_1` FOREIGN KEY (`feedback_id`) REFERENCES `feedback` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `feedbackdetail_ibfk_2` FOREIGN KEY (`categoryid`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `feedbackdetail_ibfk_3` FOREIGN KEY (`subcategoryid`) REFERENCES `subcategory` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `feedbackdetail_ibfk_4` FOREIGN KEY (`reviewtypeid`) REFERENCES `reviewtype` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `reviewtype`
--
ALTER TABLE `reviewtype`
ADD CONSTRAINT `reviewtype_ibfk_1` FOREIGN KEY (`emoji_id`) REFERENCES `emoji` (`emoji_id`);

--
-- Constraints for table `subcategory`
--
ALTER TABLE `subcategory`
ADD CONSTRAINT `subcategory_ibfk_1` FOREIGN KEY (`categoryid`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`outlet_id`) REFERENCES `outlet` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
