
var getUrl = window.location;
var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1]+"/web/";
function addnew(event,obj) {

    event.preventDefault();
    var url = obj.getAttribute("href");
    console.log(obj.text);
    var dialog = bootbox.dialog({
        title: obj.text,
        message: '<p><i class="la la-spinner"></i> Loading...</p>',
        onEscape: function() {
            // you can do anything here you want when the user dismisses dialog
            $(".sp-palette-buttons-disabled").hide();
        }

    });

    dialog.init(function(){
        var request = $.ajax({
            url: url,
            method: "GET",
        });

        request.done(function( msg ) {
            dialog.find('.bootbox-body').html(msg);
        });

        $(document).on("submit", "#form", function (event) {
            event.preventDefault();
            event.stopImmediatePropagation();

            $(this).submit(function() {
                return false;
            });



            $form = $(this); //wrap this in jQuery

            var url = $form.attr('action');

            $.ajax({
                type: "POST",
                url: url,
                data: $("#form").serialize(),
                // serializes the form's elements.
                success: function(data)
                {
                    if(data==true)
                    {
                        $(".sp-palette-buttons-disabled").hide();
                        $.pjax.defaults.timeout = 5000;
                        $.pjax.reload({container:'#p0'});

                        bootbox.hideAll();
                    }


                }
            });
            // avoid to execute the actual submit of the form.

        });

    });

};

function updateRecord(id,controller,title,event) {

    event.preventDefault();
    var dialog = bootbox.dialog({
        title: title,
        message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>',
        onEscape: function() {
            // you can do anything here you want when the user dismisses dialog
            $(".sp-palette-buttons-disabled").hide();
        }


    });


    dialog.init(function(){
        var request = $.ajax({
            url: baseUrl+controller+"/update?id="+id,
            method: "GET",
        });

        request.done(function( msg ) {
            dialog.find('.bootbox-body').html(msg);


        });

        $(document).on("submit", "#form", function (event) {
            event.preventDefault();
            event.stopImmediatePropagation();

            $(this).submit(function() {
                return false;
            });


            $form = $(this); //wrap this in jQuery

            var url = $form.attr('action');

            $.ajax({
                type: "POST",
                url: url,
                data: $("#form").serialize(),
                // serializes the form's elements.
                success: function(data)
                {
                    if(data==true)
                    {
                        //toastr.success('', 'Update Successfully', {timeOut: 2000});
                        $(".sp-palette-buttons-disabled").hide();
                        $.pjax.defaults.timeout = 5000;
                        $.pjax.reload({container:'#p0'});
                        bootbox.hideAll();
                    }
                    else
                    {
                        //toastr.success('', 'Some Error Occur', {timeOut: 2000});
                    }


                }
            });
            // avoid to execute the actual submit of the form.
        });

    });

}
