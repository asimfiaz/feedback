<?php

namespace app\helpers;

use Yii;
use yii\base\Exception;

/**
 * Created by PhpStorm.
 * User: Waqar
 * Date: 10/28/2017
 * Time: 2:35 PM
 */


class Helper
{

    public static function sendSMS($phone,$message)
    {
        $developer = 'demo';
        if($developer=='live'){
             $url = 'http://pk.eocean.us/APIManagement/API/RequestAPI?user=AL_KHAN&pwd=AHJrw8iokjlZbAuOZfODZQKP%2fIrgXMYLgmP41f1EzidaplsvO5atAkx0Q2JOi30MhA%3d%3d&sender=AL%20KHAN&reciever='.urlencode($phone).'&msg-data='.urlencode($message).'&response=string';

        }else
        {
            $url = 'http://pk.eocean.us/APIManagement/API/RequestAPI?user=AL_KHAN&pwd=AHJrw8iokjlZbAuOZfODZQKP%2fIrgXMYLgmP41f1EzidaplsvO5atAkx0Q2JOi30MhA%3d%3d&sender=AL%20KHAN&reciever='.urlencode($phone).'&msg-data='.urlencode($message).'&response=string';
        }


        $cSession = curl_init();
        curl_setopt($cSession, CURLOPT_URL, $url);
        curl_setopt($cSession, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($cSession, CURLOPT_HEADER, false);
        $result = curl_exec($cSession);
        
        curl_close($cSession);

        if (strpos($result, 'Message accepted for delivery') !== false) {
            $status = 'sent';
            return true;

        }
        else if(strpos($result, 'API Execute Successfully') !== false)
        {
            $status = 'sent';
            return true;
        }
        else{
            $status = 'failure';
            return true;
        }

    }



    public static function DateTime($dateTime)
    {
        $phpdate = strtotime( $dateTime );
        $date = date( 'd/m/Y', $phpdate );
        $time = date('h:i A',$phpdate);

        return $date.'<br>'.$time;
    }

    public static function getCreated($id)
    {
        $user = User::findOne($id);

        return  $user->username;
    }

    /**
     * Generate a name for an image that is truly unique
     * @return string The random name.
     */
    public static function generateImageName() {
        return substr(base_convert(time(), 10, 36) . md5(microtime()), 0, 16);
    }
}
?>