<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SmsText */

$this->title = 'Create Sms Text';
$this->params['breadcrumbs'][] = ['label' => 'Sms Texts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sms-text-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
