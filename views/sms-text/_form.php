<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\users */
/* @var $form yii\widgets\ActiveForm */
?>
<style>

    .btn-success {
        color: #fff;
        background-color: #5cb85c;
        border-color: #4cae4c;
        float: right;
        margin-top: 15px;
    }


</style>

<div class="sms-text-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-xs-6">
            <?= $form->field($model, 'text', [
                'template' => '<div class="col-xs-3">{label}</div> <div class="col-xs-9">{input}{error}{hint}</div>',
            ])->textarea(['rows' => '8']); ?>
        </div>

    </div>

    <div class="row">
        <div class="form-group">
            <div class="col-xs-6">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
        </div>
    </div>



    <!--<div class="row">
        <div class="col-xs-6">
            <?/*= $form->field($model, 'created_on', [
                'template' => '<div class="col-xs-3">{label}</div> <div class="col-xs-9">{input}{error}{hint}</div>',
            ]) */?>
        </div>
        <div class="col-xs-6">
            <?/*= $form->field($model, 'updated_by', [
                'template' => '<div class="col-xs-3">{label}</div> <div class="col-xs-9">{input}{error}{hint}</div>',
            ]) */?></div>
    </div>-->

    <!--<div class="row">
        <div class="col-xs-6">
            <?/*= $form->field($model, 'updated_on', [
                'template' => '<div class="col-xs-3">{label}</div> <div class="col-xs-9">{input}{error}{hint}</div>',
            ]) */?>
        </div>
        </div>-->
  



    <?php ActiveForm::end(); ?>

</div>