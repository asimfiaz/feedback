<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SmsText */

$this->title = 'Update Sms Text: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Sms Texts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sms-text-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
