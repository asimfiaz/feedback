<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SmsTextSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sms Texts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sms-text-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Sms Text', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'id',
            'text',

            ['class' => 'yii\grid\ActionColumn',
                'header'=>'Actions',
                'visibleButtons' => [


                    'delete' => function ($model) {
                        return $model->id == 0;
                    },

                ]
            ],
        ],
    ]); ?>
</div>
