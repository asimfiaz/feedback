<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Files';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="file-index">
    <?php if(!empty($_REQUEST['msg']==1)){
        echo '<div class="alert alert-success">
  <strong>Success!</strong> File Uploaded Succefully!...
</div>';
    }
    elseif (!empty($_REQUEST['msg']==2)){
        echo '<div class="alert alert-danger">
  <strong>Bad! </strong>File Must be in Mp4 or Mp3 and Size Less than 10 MB</div>';

    }

    ?>

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create File', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            'format',
            ['class' => 'yii\grid\ActionColumn',
                'header'=>'Actions',
                'visibleButtons' => [
                    'view' => function ($model) {
                        return $model->id == 0;
                    },
                    'delete' => function ($model) {
                        return $model->id == 0;
                    },

                ]
            ],
        ],
    ]); ?>
</div>
