<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\daterange\DateRangePicker;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\FeedbackSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="feedback-search">

    <?php $form = ActiveForm::begin([
        'action' => ['get-feedback-report'],
        'method' => 'get',
    ]); ?>





    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'outlet_id')
                ->dropDownList(
                    ArrayHelper::map(\app\models\Outlet::find()->asArray()->all(), 'id', 'name'),['prompt'=>'']
                )->label('Outlet'); ?>
        </div>
        <div class="col-md-6">

            <?php
            $addon = <<< HTML
<span class="input-group-addon">
    <i class="glyphicon glyphicon-calendar"></i>
</span>
HTML;



            echo '<label class="control-label">Date Range</label>';
            echo '<div class="drp-container">';
            echo DateRangePicker::widget([
                'model'=>$model,
                'attribute'=>'created_on',

                'convertFormat'=>true,

                'pluginOptions'=>[
                    'opens'=>'left',
                    'ranges' => [

                        "Today" => ["moment().startOf('day')", "moment()"],
                        "Yesterday" => ["moment().startOf('day').subtract(1,'days')", "moment().endOf('day').subtract(1,'days')"],
                        "Last 7 Days" => ["moment().startOf('day').subtract(6, 'days')", "moment()"],

                    ],

                    //'timePicker'=>true,
                    //'timePickerIncrement'=>05,
                    'locale'=>['format'=>'d/m/Y']
                ],
                'presetDropdown'=>false,
                'hideInput'=>true
            ]);
            echo '</div>'; ?>



        </div>
    </div>



    <?php // echo $form->field($model, 'created_on') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_on') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
