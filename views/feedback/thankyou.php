<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = 'Thank You :)';
$message = 'We have received your feedback and we appreciate that you’ve taken the time to write us.
You are very important to us, all information received will help us to improve our quality.
Thank You so much once again and have a nice day.';
?>
<div class="site-error text-center">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="alert alert-success">
        <?= nl2br(Html::encode($message)) ?>
    </div>

</div>
