<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $searchModel app\models\FeedbackSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Customer Wise Feedbacks');
$this->params['breadcrumbs'][] = $this->title;
?>
<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://cdn.jsdelivr.net/canvg/1.4.0/rgbcolor.js"></script>
<script src="https://code.highcharts.com/modules/cylinder.js"></script>

<div class="feedback-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',

            [
                'attribute' => 'customer_id',
                'value' => 'customer.name',
                'filter'=>ArrayHelper::map(\app\models\Customer::find()->all(),'id','name')
            ],

            [
                'attribute' => 'overall_rating',
                //'value' => 'customer.name',
                'filter'=>['1','2','3','4','5']
            ],
          //  'overall_rating',
            'remarks',
             [
                     'attribute'=>'created_on',
                 'value' => function($model) {
                     return date('d-m-Y',strtotime(\app\models\Feedback::find()->where('id='.$model->id)->one()->created_on));
                 },
             ],
            // 'created_by',
            // 'updated_on',
            // 'updated_by',
            [
                'attribute' => 'outlet_id',
                'value' => 'outlet.name',
                'filter'=>ArrayHelper::map(\app\models\Outlet::find()->all(),'id','name'),
                //'visible'=>(Yii::$app->user->identity->outlet_id==1)?1:0,
            ],

            ['class' => 'yii\grid\ActionColumn',
                'header'=>'Actions',
                'visibleButtons' => [

                    'update' => function ($model) {
                        return $model->id == 0;
                    },
                    'delete' => function ($model) {
                        return $model->id == 0;
                    },

                ]],
            ],
    ]); ?>


<?php Pjax::end(); ?></div>

<?php
$valuess =  Array();
$overall = \app\models\Feedback::find()->all();
//echo '<pre>';
// echo print_r($overall);
for ($j=5;$j>=1;$j--){
    $count=0;
    foreach ($overall as $ov){
        //echo $ov->overall_rating;
        if($ov->overall_rating==$j){
            $count+= $ov->overall_rating;
        }
    }
    $vall[] = $j.' Stars';
    $vall[] = $count/$j;
    $vall[] = false;

    $valuess[] = $vall;
    unset($vall);
    //echo $count.'\n';
}
$res = json_encode($valuess);


?>
<div class="col-md-12" id="graph"></div>

<script>

    var res = <?= $res ?>;

    Highcharts.setOptions({
        colors: ['#309b35', '#64E572', '#fdd21b', '#FD7CA7', '#d3181e', '#FF9655', '#FFF263',      '#6AF9C4']
    });

    Highcharts.chart('graph', {
        chart: {
            type: 'cylinder',
            options3d: {
                enabled: true,
                alpha: 0,
                beta: 10,
                depth: 80,
                viewDistance: 15
            }
        },
        xAxis: {
            tickInterval: 1,
            labels: {
                enabled: true,
                formatter: function() { return res[this.value][0];},
            }
        },
        yAxis: [{

            title: {
                text: 'Customers Count'
            }
        }],
        title: {
            text: 'Customer Rating Chart'
        },
        plotOptions: {
            series: {
                depth: 25,
                colorByPoint: true
            }
        },
        series: [{
            name: 'Rating',
            data: res
            //showInLegend: false
        }],
        credits: {
            enabled: false
        },
        exporting: {
            buttons: {
                contextButton: {
                    menuItems: Highcharts.getOptions().exporting.buttons.contextButton.menuItems.filter(item => item !== 'openInCloud')
    }
    }
    }
    });
</script>
