<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Feedback */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="feedback-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-lg-4">
            <?php
            $disabled = '';
            $cond = '';
            if(Yii::$app->user->identity->outlet_id!=1){
                $model->outlet_id =Yii::$app->user->identity->outlet_id;
                $disable = 'disabled';
                $cond = 'outlet_id = '.Yii::$app->user->identity->outlet_id;

            } ?>
            <?= $form->field($model, 'outlet_id', [
                'template' => '<div class="col-xs-12">{label}</div> <div class="col-xs-12">{input}{error}{hint}</div>',
            ])->dropDownList(
                ArrayHelper::map(\app\models\Outlet::find()->all(),'id','name'),
                ['prompt'=>'Select Outlet','class'=>'form-control',$disable=>'disabled','data-url'=>\Yii::$app->urlManager->createUrl(['site/getcustomers'])]
            )?>


        </div>
        <!--<div class="col-lg-4">
            <?/*= $form->field($model, 'customer_id', [
                'template' => '<div class="col-xs-12">{label} '.Html::a('Not in the list? Add new', ['/site/getcustomers'], ['style'=>'float: right']).' </div> <div class="col-xs-12">{input}{error}{hint}</div>',
            ])->dropDownList(
                ArrayHelper::map(\app\models\Customer::find()->where($cond)->all(),'id','name'),
                ['prompt'=>'Select Customer','class'=>'form-control']
            )*/?>
        </div>-->
        <div class="col-lg-4">
            <?= $form->field($model, 'overall_rating', [
                'template' => '<div class="col-xs-12">{label}</div> <div class="col-xs-12">{input}{error}{hint}</div>',
            ])->dropDownList(['1'=>'1 Star','2'=>'2 Star','3'=>'3 Star','4'=>'4 Star','5'=>'5 Star',],
                ['prompt'=>'Select Overall Rating','class'=>'form-control']
            )?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-4">
            <?= $form->field($model, 'remarks', [
                'template' => '<div class="col-xs-12">{label}</div> <div class="col-xs-12">{input}{error}{hint}</div>',
            ])->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-4">
            <label class="col-lg-12" style=" height: 18px;"></label>
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered table-responsive table-condensed">
                <thead>
                    <tr>
                        <th>Complaint Category</th>
                        <th>&nbsp;</th>
                        <?php $revtype = \app\models\Reviewtype::find()->where('status=1')->all(); //echo '<pre>'; print_r($revtype);
                        foreach($revtype as $v){
                            ?>
                            <th><?= $v->name?></th>
                        <?php
                        }
                        ?>
                    </tr>
                </thead>
                <tbody>
                <?php $cat = \app\models\Category::find()->where('status=1')->all();
                foreach($cat as $k=>$v){
                    $subcat = \app\models\Subcategory::find()->where('categoryid = '.$v->id.' and status=1')->all();
                    ?>

                <?php foreach($subcat as $d=>$p){ ?>
                    <tr>
                        <?php if($subcat[$d]['categoryid']!=$subcat[$d-1]['categoryid']){
                            ?>
                            <td class="centercontent" rowspan="<?= count($subcat)?>"><?= $v->cat_name ?></td>
                        <?php
                        } ?>
                            <td><?= $p->name ?></td>
                        <?php $revtype = \app\models\Reviewtype::find()->where('status=1')->all(); //echo '<pre>'; print_r($revtype);
                        foreach($revtype as $m){
                            ?>
                            <td>
                                <div class="radio">
                                    <label><input type="radio" id='subcatradio_<?= $v->id.'-'.$p->id.'-'.$m->id ?>' value="<?= $v->id.'-'.$p->id.'-'.$m->id ?>" name="subcatradio[<?= $p->id ?>]"></label>
                                </div>
                            </td>
                        <?php
                        }
                        ?>
                    </tr>
                <?php } ?>
                <?php
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
