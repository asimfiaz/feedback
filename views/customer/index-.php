<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $searchModel app\models\CustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Customers');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customer-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <!--<p>
        <?/*= Html::a(Yii::t('app', 'Create Customer'), ['create'], ['class' => 'btn btn-success']) */?>
    </p>-->
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //'id',
            'name',
            'contact_no',

            [
                'attribute' => 'birthdate',
                'value'=>function($model) {
                    $birth = \app\models\Customer::find()->where('status=1 and id='.$model->id)->orderBy(['(id)' => SORT_DESC]) ->one();
                    if($birth->birthdate==NULL){
                        return "Not Set";
                    }
                    else{
                        return (date('d-m-Y',strtotime($birth->birthdate)));
                    }

                },
            ],
            [
                'header' => 'Outlet Name',
                'attribute' => 'outlet_id',
                'value' => 'outlet.name',
                'filter'=>ArrayHelper::map(\app\models\Outlet::find()->all(),'id','name'),
                // 'visible'=>(Yii::$app->user->identity->outlet_id==1)?1:0,
            ],

            /*[
                'attribute' => 'email',
                'value'=>function($model) {
                    $email = \app\models\Customer::find()->where('status=1 and id='.$model->id)->orderBy(['(id)' => SORT_DESC]) ->one();
                    if($email->email==NULL){
                        return "Not Set";
                    }
                    else{
                        return $email->email;
                    }

                },
            ],*/

           /* [
                'attribute' => 'sms_promotion',
                'header' =>'Sms Promo',
                'value' =>function($data){
                    if($data['sms_promotion']==1){
                        return 'Yes';
                    }
                    else
                        return 'No';
                },
                'filter'=>array('1'=>'Yes','0'=>'No')

            ],*/

            /*[
                'attribute' => 'link_to_facebook',
                'header' =>'FB Promo',
                'value' =>function($data){
                    if($data['link_to_facebook']==1){
                        return 'Yes';
                    }
                    else
                        return 'No';
                },
                'filter'=>array('1'=>'Yes','0'=>'No')

            ],*/
            // 'sms_promotion',
            // 'link_to_facebook',
            // 'facebook_link',
            /*[
                'attribute' => 'status',
                'header' =>'Status',
                'value' =>function($data){
                    if($data['status']==1){
                        return 'Active';
                    }
                    else
                        return 'Inactive';
                },
                'filter'=>array('1'=>'Active','0'=>'Inactive')

            ],*/
            // 'created_on',
            // 'created_by',
            // 'updated_on',
            // 'updated_by',

           // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
