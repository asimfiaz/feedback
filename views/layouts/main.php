<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\User;
use app\models\Outlet;

AppAsset::register($this);

if (!empty($_SESSION['__id'])) {
    $user = User::findIdentity($_SESSION['__id']);

    $outlet = Outlet::find()
        ->where('id', $user->outlet_id)
        ->one();
} else {
    $outlet = new Outlet();
}

$logo = empty($outlet->logo) ? "logo.png" : $outlet->logo;
?>

<script src="<?= Yii::$app->request->baseUrl.'/js/jquery-3.2.1.min.js' ?>"></script>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <a class="navbar-brand" href="#">
        <img src="<?= \Yii::getAlias('@web').'/frontend/'.$logo ?>" class="img-responsive">
    </a>
    <?php
    NavBar::begin([
        'brandLabel' => Html::img('@web/frontend/'.$logo, ['alt'=>Yii::$app->name,'class' => 'img-responsive']),
        'options' => ['class' => 'navbar-inverse navbar-static-top'],//options of the navbar
        'brandUrl' => Yii::$app->homeUrl
    ]);
    if(!empty(Yii::$app->user->identity->name)&&isset(Yii::$app->user->identity->name)){
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Home', 'url' => ['/site/home']],
            ['label' => 'Feedback', 'url' => ['/site/index']],
            ['label' => 'Customer', 'url' => ['/customer/index']],
            [
                'label' => 'Reports',
                'items' => [
                    ['label' => 'Customer wise Feedback', 'url' => ['/feedback/index']],
                    ['label' => 'Feedback Report', 'url' => ['/feedback/get-feedback-report']],
                    ['label' => 'Customer Visit Report', 'url' => ['/customer/get-customer-report']],
                    ['label' => 'Overall Chart Report', 'url' => ['site/chart-report']],
                ],
            ],
            [
                'label' => 'Settings',
                'items' => [
                    ['label' => 'Category', 'url' => ['/category/index']],
                    ['label' => 'Sub Category', 'url' => ['/subcategory/index']],
                    //['label' => 'Review Type', 'url' => ['/reviewtype/index']],
                    '<li class="divider"></li>',
                    ['label' => 'Outlet', 'url' => ['/outlet/index']],
                    ['label' => 'Users', 'url' => ['/users/index']],
                    ['label' => 'SMS Text', 'url' => ['/sms-text/index']],
                    ['label' => 'File Upload', 'url' => ['/file/index']],
                    ['label' => 'Emojis', 'url' => ['/emoji/index']],
                    ['label' => 'Review Types', 'url' => ['/reviewtype/index']],
                ],
            ],
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'get')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->name . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    }
    NavBar::end();
    
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Maalik Creative Engineering, <?= date('Y') ?></p>

<!--        <p class="pull-right">--><?//= Yii::powered() ?><!--</p>-->
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
