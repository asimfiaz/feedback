<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Emoji */

$this->title = $model->emoji_id;
$this->params['breadcrumbs'][] = ['label' => 'Emojis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="emoji-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->emoji_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->emoji_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'emoji_id',
            'emoji_name',
            [
                'attribute' => 'emoji_content:ntext',
                'format' => 'html',
                'label' => 'Content',
                'value' => function ($data) {
                    return Html::img(
                        Yii::$app->homeUrl.'frontend/' . $data['emoji_content'],
                        ['width' => '50px']
                    );
                },
            ],
        ],
    ]) ?>

</div>
