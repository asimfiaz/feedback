<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Emoji */

$this->title = 'Create Emoji';
$this->params['breadcrumbs'][] = ['label' => 'Emojis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="emoji-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
