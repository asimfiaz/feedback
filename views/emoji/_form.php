<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Emoji */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="emoji-form">
  <?php $form = ActiveForm::begin([
    'options' => ['enctype' => 'multipart/form-data']
  ]); ?>

  <?= $form->field($model, 'emoji_name')->textInput(['maxlength' => true]) ?>
  <?= $form->field($model, 'emoji_content')->fileInput() ?>

  <div class="form-group">
    <?= Html::submitButton(
      $model->isNewRecord ? 'Create' : 'Update', [
        'class' => $model->isNewRecord ? 
          'btn btn-success' : 'btn btn-primary'
      ]
    ) ?>
  </div>

  <?php ActiveForm::end(); ?>
</div>