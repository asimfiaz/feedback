<?php

/* @var $this yii\web\View */

$this->title = 'Feedback';
//print_r($cat);exit();
?>

<embed src="<?= Yii::$app->homeUrl ?>frontend/elisyam/img/feedback_background_music.mp3" autostart="true" preload="auto" width="2" height="0">

<div class="widget-body" id="top">
  <div class="row flex-row justify-content-center">
    <div class="col-xl-10">
      <form class="needs-validation" novalidate id="commentForm" method="post" action="<?= Yii::$app->homeUrl ?>site/thankyou" class="form-horizontal">
        <input type="hidden" name="subcatradio" id="subcatradio" class="required" />
        <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
        <input type="hidden" name="outlet" value="<?= Yii::$app->user->identity->outlet_id ?>" />

        <div id="rootwizard">
          <div class="step-container">
            <div class="step-wizard" id="bar">
              <div class="progress">
                <div class="progressbar"></div>
              </div>
              <ul>
                <?php

                $i = 0;
                foreach ($cat as $catRow) {
                ?>
                  <li><a href="#tab<?= ++$i ?>" data-toggle="tab"></a></li>
                <?php
                }
                ?>
                <li><a href="#tab<?= ++$i ?>" data-toggle="tab"></a></li>
                <li><a href="#tab<?= ++$i ?>" data-toggle="tab"></a></li>
              </ul>
            </div>
          </div>
          <div class="tab-content">
            <!-- Customer Details -->
            <div class="tab-pane" id="tab1">
              <h2 class="page-header-title">Customer Detail</h2>
              <br><br><br>

              <div class="widget-body">
                <!-- Name -->
                <div class="form-group row d-flex align-items-center mb-5">
                  <label class="col-lg-4 form-control-label d-flex justify-content-lg-end">Name*</label>
                  <div class="col-lg-5">
                    <input type="text" class="form-control required" id="name" name="name" placeholder="Enter your name">
                  </div>
                </div>
                <!-- Phone -->
                <div class="form-group row d-flex align-items-center mb-5">
                  <label class="col-lg-4 form-control-label d-flex justify-content-lg-end">Mobile # *</label>
                  <div class="col-lg-5">
                    <input type="text" class="form-control required" 
                      id="phone" name="phone" 
                      data-inputmask="'mask': '9999-9999999'">
                  </div>
                </div>

                <!-- Email -->
                <div class="form-group row d-flex align-items-center mb-5">
                  <label class="col-lg-4 form-control-label d-flex justify-content-lg-end">E-Mail</label>
                  <div class="col-lg-5">
                    <input type="text" class="form-control" 
                      id="email" name="email" 
                      title="Contact's email (format: xxx@xxx.xxx)"
                      data-inputmask="'alias': 'email'"
                      inputmode="email">
                  </div>
                </div>
                <!-- Date of birth -->
                <div class="form-group row d-flex align-items-center mb-5">
                  <label class="col-lg-4 form-control-label d-flex justify-content-lg-end">Date of Birth</label>
                  <div class="col-lg-5">
                    <input class="form-control from" 
                      id="dob" name="dob" 
                      data-inputmask-alias="datetime" 
                      data-inputmask-inputformat="dd-mm-yyyy" 
                      inputmode="numeric">
                  </div>
                </div>
              </div>
            </div>

            <div class="form-group row d-flex align-items-center mb-5">
              <label class="col-lg-4 form-control-label d-flex justify-content-lg-end"></label>
              <div class="col-lg-5">
                <div for="subcatradio" id="herror" class="error" style="color: red;text-align: left;font-size: 14px;margin-top: 15px">Please Select one of above Option</div>
              </div>
            </div>

            <?php
            $i = 1;
            foreach ($cat as $catRow) {
              ?>
              <!-- <?= $catRow['cat_name'] ?> -->
              <div class="tab-pane" id="tab<?= ++$i ?>">
                <h2 class="page-header-title"><?= $catRow['cat_name'] ?></h2>
                <br><br><br>

                <div class="table-responsive">
                  <table class="table mb-0">
                    <thead>
                      <tr>
                        <th>Item</th>
                        <?php
                        foreach ($revtype as $revRow) {
                          ?>
                          <th>
                            <?= $revRow['name'] ?>
                          </th>
                          <?php
                        }
                        ?>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      $subcat = \app\models\Subcategory::find()
                      ->where('status=1')
                      ->where('categoryid=' . $catRow['id'])
                      ->all();

                      foreach ($subcat as $subcatRow) {
                        ?>
                        <tr>
                          <td><?= $subcatRow['name'] ?></td>
                          <?php
                          foreach ($revtype as $revRow) {
                            ?>
                            <td onclick="restoreOpacity(this);">
                              <label class="checkbox-box">
                                <input type="radio" id="subcatradio_<?= $catRow['id'] . '-' . $subcatRow['id'] . '-' . $revRow['id'] ?>" value="<?= $catRow['id'] . '-' . $subcatRow['id'] . '-' . $revRow['id'] ?>" name="subcatradio[<?= $subcatRow['id'] ?>]">

                                <i class="em" onclick="document.getElementById('subcatradio_<?= $catRow['id'] . '-' . $subcatRow['id'] . '-' . $revRow['id'] ?>').checked = true;" style="width: 50px; 
                                  height: 50px; 
                                  background-image:url(
                                    '<?= Yii::$app->homeUrl ?>frontend/<?= $revRow['emoji_content'] ?>'
                                  );">
                                </i>

                              </label>
                            </td>
                            <?php
                          }
                          ?>
                        </tr>
                        <?php
                      }
                      ?>

                    </tbody>
                  </table>
                </div>

              </div>
              <?php
            }
            ?>

            <div class="tab-pane" id="tab<?= ++$i ?>">
              <h2 class="page-header-title">Comment Something</h2>
              <div style="padding:20px;">
                <textarea id="detail" rows="7" name="remarks" placeholder="Write Some Comments Here........." class="form-control"></textarea>
              </div>
              <h2 class="page-header-title">Over All Rating</h2>
              <br>
              <br>
              <br>


              <div class="row">
                <div class="col-12">
                  <div class='rating-stars text-center'>
                    <ul id='stars'>
                      <li class='star' title='Poor' data-value='1'>
                        <i class='fa fa-star fa-fw'></i>
                      </li>
                      <li class='star' title='Fair' data-value='2'>
                        <i class='fa fa-star fa-fw'></i>
                      </li>
                      <li class='star' title='Good' data-value='3'>
                        <i class='fa fa-star fa-fw'></i>
                      </li>
                      <li class='star' title='Excellent' data-value='4'>
                        <i class='fa fa-star fa-fw'></i>
                      </li>
                      <li class='star' title='WOW!!!' data-value='5'>
                        <i class='fa fa-star fa-fw'></i>
                      </li>
                    </ul>
                  </div>
                  <div class='success-box'>
                    <div class='clearfix'></div>
                    <div class='text-message'></div>
                    <div class='clearfix'></div>
                  </div>
                  <input type="hidden" name="myRate1" id="myRate1" />
                </div>
              </div>

            </div>

            <ul class="pager wizard text-center" style="margin-top: 25px">
              <li class="previous d-inline-block">
                <a href="javascript:;" class="btn btn-secondary ripple">Previous</a>
              </li>
              <li onclick="location.href='#top';" class="next d-inline-block">
                <a href="javascript:;" class="btn btn-gradient-01">Next</a>
              </li>
              <li class="next d-inline-block">
                <input type="submit" class="finish btn btn-gradient-01" id="finish" style="display: none" />
              </li>
            </ul>
          </div>
      </form>
    </div>
  </div>
</div>

<style>
  .step-wizard .progress {
    top: 0px !important;
  }

  * {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
  }

  *:before,
  *:after {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
  }

  .clearfix {
    clear: both;
  }

  .text-center {
    text-align: center;
  }

  a {
    color: tomato;
    text-decoration: none;
  }

  a:hover {
    color: #2196f3;
  }

  pre {
    display: block;
    padding: 9.5px;
    margin: 0 0 10px;
    font-size: 13px;
    line-height: 1.42857143;
    color: #333;
    word-break: break-all;
    word-wrap: break-word;
    background-color: #F5F5F5;
    border: 1px solid #CCC;
    border-radius: 4px;
  }

  .header {
    padding: 20px 0;
    position: relative;
    margin-bottom: 10px;

  }

  .header:after {
    content: "";
    display: block;
    height: 1px;
    background: #eee;
    position: absolute;
    left: 30%;
    right: 30%;
  }

  .header h2 {
    font-size: 3em;
    font-weight: 300;
    margin-bottom: 0.2em;
  }

  .header p {
    font-size: 14px;
  }



  #a-footer {
    margin: 20px 0;
  }

  .new-react-version {
    padding: 20px 20px;
    border: 1px solid #eee;
    border-radius: 20px;
    box-shadow: 0 2px 12px 0 rgba(0, 0, 0, 0.1);

    text-align: center;
    font-size: 14px;
    line-height: 1.7;
  }

  .new-react-version .react-svg-logo {
    text-align: center;
    max-width: 60px;
    margin: 20px auto;
    margin-top: 0;
  }

  .success-box {
    margin: 15px 0;
  }

  .success-box img {
    margin-right: 10px;
    display: inline-block;
    vertical-align: top;
  }

  .text-message {
    color: #fe195e;
  }

  .success-box>div {
    vertical-align: top;
    display: inline-block;
    color: #888;
  }

  /* Rating Star Widgets Style */
  .rating-stars ul {
    list-style-type: none;
    padding: 0;
    list-style-position: inside;

    -moz-user-select: none;
    -webkit-user-select: none;
  }

  .rating-stars ul>li.star {
    display: inline-block;

  }

  /* Idle State of the stars */
  .rating-stars ul>li.star>i.fa {
    font-size: 2.5em;
    /* Change the size of the stars */
    color: #ccc;
    /* Color on idle state */
  }

  /* Hover state of the stars */
  .rating-stars ul>li.star.hover>i.fa {
    color: #FFCC36;
  }

  /* Selected state of the stars */
  .rating-stars ul>li.star.selected>i.fa {
    color: #FF912C;
  }

  .star:after {
    content: "";
  }

  /* Customize the label (the container) */
  .checkbox-box {
    display: block;
    position: relative;
    padding-left: 35px;
    margin-bottom: 12px;
    cursor: pointer;
    font-size: 22px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
  }

  /* Hide the browser's default radio button */
  .checkbox-box input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
    height: 0;
    width: 0;
  }

  /* Create a custom radio button */
  .checkmark {
    position: absolute;
    top: -8px;
    left: 45%;
    height: 25px;
    width: 25px;
    background-color: #eee;
    border-radius: 50%;
    border: 1px solid #eb3235;
  }

  /* On mouse-over, add a grey background color */
  .checkbox-box:hover input~.checkmark {
    background-color: #ccc;
  }

  /* When the radio button is checked, add a blue background */
  .checkbox-box input:checked~.checkmark {
    background-color: #eb3235;
  }

  /* Create the indicator (the dot/circle - hidden when not checked) */
  .checkmark:after {
    content: "";
    position: absolute;
    display: none;
  }

  /* Show the indicator (dot/circle) when checked */
  .checkbox-box input:checked~.checkmark:after {
    display: block;
  }

  /* Style the indicator (dot/circle) */
  .checkbox-box .checkmark:after {
    top: 8px;
    left: 8px;
    width: 8px;
    height: 8px;
    border-radius: 50%;
    background: white;
  }


  form label {
    color: #fe195e;

  }

  form label.error {
    font-size: .95rem;
    padding-top: 5px;
    float: left;

  }

  .error {
    background: #FFFF00;
  }


  @media (max-width: 900px) {

    table.emoji-table {
      margin-left: 200px !important;
    }

  }

  @media (min-width: 768px) {

    table.emoji-table {
      margin-left: 300px !important;
    }

  }

  @media only screen and (max-width: 860px) {
    table.emoji-table {
      margin-left: 170px !important;
    }
  }

  @media only screen and (max-width: 767px) {
    table.emoji-table {
      margin-left: 150px !important;
    }
  }

  @media only screen and (max-width: 650px) {
    table.emoji-table {
      margin-left: 80px !important;
    }
  }

  @media only screen and (max-width: 450px) {
    table.emoji-table {
      margin-left: 0px !important;
    }
  }
</style>

<script>
  $(document).ready(function() {
    //$(":input").inputmask();

    $.noConflict();
    //var table = $('# your selector').DataTable();

    /* 1. Visualizing things on Hover - See next part for action on click */
    $('#stars li').on('mouseover', function() {
      var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

      // Now highlight all the stars that's not after the current hovered star
      $(this).parent().children('li.star').each(function(e) {
        if (e < onStar) {
          $(this).addClass('hover');
        } else {
          $(this).removeClass('hover');
        }
      });

    }).on('mouseout', function() {
      $(this).parent().children('li.star').each(function(e) {
        $(this).removeClass('hover');
      });
    });

    /* 2. Action to perform on click */
    $('#stars li').on('click', function() {
      var onStar = parseInt($(this).data('value'), 10); // The star currently selected
      var stars = $(this).parent().children('li.star');

      for (i = 0; i < stars.length; i++) {
        $(stars[i]).removeClass('selected');
      }

      for (i = 0; i < onStar; i++) {
        $(stars[i]).addClass('selected');
      }

      // JUST RESPONSE (Not needed)
      var ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
      var msg = "";
      if (ratingValue > 1) {
        msg = "Thanks! You rated this " + ratingValue + " stars.";
      } else {
        msg = "We will improve ourselves. You rated this " + ratingValue + " stars.";
      }
      document.getElementById("myRate1").value = ratingValue;
      responseMessage(msg);

    });

    function responseMessage(msg) {
      $('.success-box').fadeIn(200);
      $('.success-box div.text-message').html("<span>" + msg + "</span>");
    }

    $('.from').datepicker({
      format: "dd-mm-yyyy",
      weekStart: 0,
      autoclose: true,
      todayHighlight: true
    });

    var $validator = $("#commentForm").validate({
      rules: {
        name: {
          required: true,
        },
        phone: {
          required: true,
        },
      },
      messages: {
        name: {
          required: "Please Enter Name",
        },
        phone: {
          required: "Please Enter Mobile #",
        },
      },
    });

    $('#rootwizard').bootstrapWizard({
      'tabClass': 'nav nav-pills',
      'onNext': function(f, a, d) {
        var $valid = $("#commentForm").valid();

        if (!$valid) {
          $validator.focusInvalid();
          return false;
        }
      },
      'onTabShow': function(f, a, d) {

        var c = a.find("li").length;
        var e = d + 1;
        var b = (e / c) * 100;
        $("#rootwizard .progressbar").css({
          width: b + "%"
        });
        if (b == 100) {
          $('#next').hide();
          $("#finish").show();
        } else {
          $('#next').show();
          $("#finish").hide();
        }
      }

    });

    $('#herror').hide();
    $('#next').on('click', function() {
      var cat = $('#subcatradio').val();

      if (cat == '') {
        $('#herror').show();
      } else {
        $('#herror').hide();
      }
    });

    $("input[name*='subcatradio']").each(function() {
      $(this).rules('add', {
        required: true,
        messages: {
          required: "required"
        },
      });
    });

  });
</script>

<script>
  function reduceOpacity(item) {
    item.style.opacity = '60%';
  }

  function restoreOpacity(item) {
    item.style.opacity = '100%';

    // restore opacity of all other emojis in the row
    var parent = item.parentNode;
    var childs = parent.children;

    for (var i = 0; i < childs.length; i++) {
      if (childs[i] != item) {
        reduceOpacity(childs[i]);
      }
    }
  }

</script>

<?php 
  const MAX_WIDTH = '760px';
  const MIN_DEVICE_WIDTH = '768px';
  const MAX_DEVICE_WIDTH = '1024px';
?>
<style>
  /* 
    Max width before this PARTICULAR table gets nasty
    This query will take effect for any screen smaller than 760px
    and also iPads specifically.
  */
  @media only screen and 
  (max-width: <?= MAX_WIDTH ?>),
  (min-device-width: <?= MIN_DEVICE_WIDTH ?>) and 
  (max-device-width: <?= MAX_DEVICE_WIDTH ?>)  {
    /* Force table to not be like tables anymore */
    table, thead, tbody, th, td, tr { 
      display: block; 
    }
    
    /* Hide table headers (but not display: none;, for accessibility) */
    thead tr { 
      position: absolute;
      top: -9999px;
      left: -9999px;
    }
    
    tr { border: 1px solid #ccc; }
    
    td { 
      /* Behave  like a "row" */
      border: none;
      border-bottom: 1px solid #eee; 
      position: relative;
      padding-left: 50%; 
    }
    
    td:before { 
      /* Now like a table header */
      position: absolute;
      /* Top/left values mimic padding */
      top: 6px;
      left: 6px;
      width: 45%; 
      padding-right: 10px; 
      white-space: nowrap;
    }
    
    /*
    Label the data
    td:nth-of-type(1):before { content: "Item"; }
    */
  
  }
</style>

<?php
  $i = 1;
  foreach ($revtype as $revRow) {
    ?>
    <style>
      @media only screen and 
      (max-width: <?= MAX_WIDTH ?>),
      (min-device-width: <?= MIN_DEVICE_WIDTH ?>) and 
      (max-device-width: <?= MAX_DEVICE_WIDTH ?>) {
        td:nth-of-type(<?= ++$i ?>):before { 
          content: "<?= $revRow['name'] ?>"; 
          margin-top: 25px;
        }
      }
    </style>
    <?php
  }
?>