
<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>

<style>

* {
        -webkit-box-sizing:border-box;
        -moz-box-sizing:border-box;
        box-sizing:border-box;
    }

    *:before, *:after {
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
    }

    .clearfix {
        clear:both;
    }

    .text-center {text-align:center;}

    a {
        color: tomato;
        text-decoration: none;
    }

    a:hover {
        color: #2196f3;
    }

    pre {
        display: block;
        padding: 9.5px;
        margin: 0 0 10px;
        font-size: 13px;
        line-height: 1.42857143;
        color: #333;
        word-break: break-all;
        word-wrap: break-word;
        background-color: #F5F5F5;
        border: 1px solid #CCC;
        border-radius: 4px;
    }

    .header {
        padding:20px 0;
        position:relative;
        margin-bottom:10px;

    }

    .header:after {
        content:"";
        display:block;
        height:1px;
        background:#eee;
        position:absolute;
        left:30%; right:30%;
    }

    .header h2 {
        font-size:3em;
        font-weight:300;
        margin-bottom:0.2em;
    }

    .header p {
        font-size:14px;
    }



    #a-footer {
        margin: 20px 0;
    }

    .new-react-version {
        padding: 20px 20px;
        border: 1px solid #eee;
        border-radius: 20px;
        box-shadow: 0 2px 12px 0 rgba(0,0,0,0.1);

        text-align: center;
        font-size: 14px;
        line-height: 1.7;
    }

    .new-react-version .react-svg-logo {
        text-align: center;
        max-width: 60px;
        margin: 20px auto;
        margin-top: 0;
    }





    .success-box {
        margin:50px 0;
        padding:10px 10px;
        border:1px solid #eee;
        background:#f9f9f9;
    }

    .success-box img {
        margin-right:10px;
        display:inline-block;
        vertical-align:top;
    }

    .success-box > div {
        vertical-align:top;
        display:inline-block;
        color:#888;
    }



    /* Rating Star Widgets Style */
    .rating-stars ul {
        list-style-type:none;
        padding:0;
        list-style-position: inside;

        -moz-user-select:none;
        -webkit-user-select:none;
    }
    .rating-stars ul > li.star {
        display:inline-block;

    }

    /* Idle State of the stars */
    .rating-stars ul > li.star > i.fa {
        font-size:2.5em; /* Change the size of the stars */
        color:#ccc; /* Color on idle state */
    }

    /* Hover state of the stars */
    .rating-stars ul > li.star.hover > i.fa {
        color:#FFCC36;
    }

    /* Selected state of the stars */
    .rating-stars ul > li.star.selected > i.fa {
        color:#FF912C;
    }

/* Customize the label (the container) */
.checkbox-box {
  display: block;
  position: relative;
  padding-left: 35px;
  margin-bottom: 12px;
  cursor: pointer;
  font-size: 22px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

/* Hide the browser's default radio button */
.checkbox-box input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
  height: 0;
  width: 0;
}

/* Create a custom radio button */
.checkmark {
  position: absolute;
  top: -8px;
  left: 41%;
  height: 25px;
  width: 25px;
  background-color: #eee;
  border-radius: 50%;
   border: 1px solid #eb3235;
}

/* On mouse-over, add a grey background color */
.checkbox-box:hover input ~ .checkmark {
  background-color: #ccc;
}

/* When the radio button is checked, add a blue background */
.checkbox-box input:checked ~ .checkmark {
  background-color: #eb3235;
}

/* Create the indicator (the dot/circle - hidden when not checked) */
.checkmark:after {
  content: "";
  position: absolute;
  display: none;
}

/* Show the indicator (dot/circle) when checked */
.checkbox-box input:checked ~ .checkmark:after {
  display: block;
}

/* Style the indicator (dot/circle) */
.checkbox-box .checkmark:after {
  top: 8px;
  left: 8px;
  width: 8px;
  height: 8px;
  border-radius: 50%;
  background: white;
}

	form {

    }
    form label {
        color: #fe195e;
    }
    form label.error{
        font-size: .95rem;
        padding-top: 5px;
    }
</style>


	<embed src="<?=Yii::$app->homeUrl?>elisyam/img/feedback_background_music.mp3" autostart="true" preload="auto" loop="true" width="2" height="0">
   
	<div class="widget-body">
    <div class="row flex-row justify-content-center">
        <div class="col-xl-10">
		<form class="needs-validation" novalidate id="commentForm" method="get" action="" class="form-horizontal">
	            <div id="rootwizard">
    <div class="step-container">
        <div class="step-wizard" id="bar">
            <div class="progress">
                <div class="progressbar"></div>
            </div>
            <ul>
                <li><a href="#tab1" data-toggle="tab"></a></li>
                <li><a href="#tab2" data-toggle="tab"></a></li>
                <li><a href="#tab3" data-toggle="tab"></a></li>
                <li><a href="#tab4" data-toggle="tab"></a></li>
				<li><a href="#tab5" data-toggle="tab"></a></li>
            </ul>
        </div>
    </div>
    <div class="tab-content">
        <div class="tab-pane" id="tab1">
            <h2 class="page-header-title">Customer Detail</h2>
            <br>
            <br>
            <br>
            <div class="widget-body">
                
                    <div class="form-group row d-flex align-items-center mb-5">
                        <label class="col-lg-4 form-control-label d-flex justify-content-lg-end">Name</label>
                        <div class="col-lg-5">
                            <input type="text" class="form-control required" id="name" name="name" placeholder="Enter your name">
                        </div>
                    </div>
                   
					<div class="form-group row d-flex align-items-center mb-5">
                        <label class="col-lg-4 form-control-label d-flex justify-content-lg-end">Mobile # *</label>
                        <div class="col-lg-5">
                                <input type="text" class="form-control required" id ="phone" name ="phone" placeholder="Mobile Number">
                        </div>
                    </div>
                    
                    <!--<div class="form-group row d-flex align-items-center mb-5">
                        <label class="col-lg-4 form-control-label d-flex justify-content-lg-end">Mob #</label>
                        <div class="col-lg-5">
                            <div class="input-group">
                                <span class="input-group-addon addon-primary">
                                    <i class="la la-phone"></i>
                                </span>
                                <input type="text" class="form-control" placeholder="Phone number">
                            </div>
                        </div>
                    </div> -->
					
					<div class="form-group row d-flex align-items-center mb-5">
                        <label class="col-lg-4 form-control-label d-flex justify-content-lg-end">Date of Birth</label>
                        <div class="col-lg-5">
                            <input type="text" class="form-control from" id ="dob"  name= "dob" placeholder="" >

                        </div>
                    </div>
					
					<!-- <div class="form-group row d-flex align-items-center mb-5">
                        <label class="col-lg-4 form-control-label d-flex justify-content-lg-end">Date of Birth</label>
                        <div class="col-lg-2">
                            <input type="text" class="form-control" placeholder="Date">
                        </div>
						<div class="col-lg-2">
                            <input type="text" class="form-control" placeholder="Month">
                        </div>
						<div class="col-lg-2">
                            <input type="text" class="form-control" placeholder="Year">
                        </div>
                    </div> -->
					
					
         
            </div>
        </div>
        <div class="tab-pane" id="tab2">
            <h2 class="page-header-title">FOOD</h2><br><br><br>
            <div class="table-responsive">
                <table class="table mb-0">
                    <thead>
                    <tr>
                        <th>Item</th>
                        <th>Excellent</th>
                        <th>Good</th>
                        <th>Fair</th>
                        <th>Poor</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Menu Variety</td>
                        <td>
						 <label class="checkbox-box">
								<input type="radio" id='subcatradio_1-2-01' value="1-2-01" name="subcatradio[1]">
								<span class="checkmark"></span>
								</label>
								
                           
                        </td>
                        <td>
                            <label class="checkbox-box">
								<input type="radio" id='subcatradio_1-2-02' value="1-2-02" name="subcatradio[1]">
								<span class="checkmark"></span>
								</label>
                        </td>
                        <td>
                            <label class="checkbox-box">
								<input type="radio" id='subcatradio_1-2-03' value="1-2-03" name="subcatradio[1]">
								<span class="checkmark"></span>
								</label>
                        </td>
                        <td>
                            <label class="checkbox-box">
								<input type="radio" id='subcatradio_1-2-04' value="1-2-04" name="subcatradio[1]">
								<span class="checkmark"></span>
								</label>
                        </td>
                    </tr>
					
                    <tr>
                        <td>Presentation</td>
                        <td>
                            
                                <label class="checkbox-box">
								<input type="radio" id='subcatradio_1-2-1' value="1-2-1" name="subcatradio[2]">
								<span class="checkmark"></span>
								</label>
                            
                        </td>
                        <td>
                            
							 <label class="checkbox-box">
								<input type="radio" id='subcatradio_1-2-2' value="1-2-2" name="subcatradio[2]"checked="checked" >
								<span class="checkmark"></span>
								</label>
							
							
                        </td>
                        <td>
                            <label class="checkbox-box">
								<input type="radio" id='subcatradio_1-2-3' value="1-2-3" name="subcatradio[2]">
								<span class="checkmark"></span>
								</label>
                        </td>
                        <td>
                             <label class="checkbox-box">
								<input type="radio" id='subcatradio_1-2-4' value="1-2-4" name="subcatradio[2]">
								<span class="checkmark"></span>
								</label>
                        </td>
                    </tr>
                    <tr>
                        <td>Serving Size</td>
                        <td>
							<label class="checkbox-box">
								<input type="radio" id='subcatradio_1-3-1' value="1-3-1" name="subcatradio[3]">
								<span class="checkmark"></span>
							</label>
							
                            
                        </td>
                        <td>
                            <label class="checkbox-box">
								<input type="radio" id='subcatradio_1-3-2' value="1-3-2" name="subcatradio[3]">
								<span class="checkmark"></span>
							</label>
                        </td>
                        <td>
                            <label class="checkbox-box">
								<input type="radio" id='subcatradio_1-3-3' value="1-3-3" name="subcatradio[3]">
								<span class="checkmark"></span>
							</label>
                        </td>
                        <td>
                            <label class="checkbox-box">
								<input type="radio" id='subcatradio_1-3-4' value="1-3-4" name="subcatradio[3]">
								<span class="checkmark"></span>
							</label>
                        </td>
                    </tr>
                    <tr>
                        <td>Temperature</td>
                        <td>
                            <label class="checkbox-box">
								<input type="radio" id='subcatradio_1-4-1' value="1-4-1" name="subcatradio[4]">
								<span class="checkmark"></span>
							</label>
                        </td>
                        <td>
                            <label class="checkbox-box">
								<input type="radio" id='subcatradio_1-4-2' value="1-4-2" name="subcatradio[4]">
								<span class="checkmark"></span>
							</label>
                        </td>
                        <td>
                            <label class="checkbox-box">
								<input type="radio" id='subcatradio_1-4-3' value="1-4-3" name="subcatradio[4]">
								<span class="checkmark"></span>
							</label>
                        </td>
                        <td>
                           <label class="checkbox-box">
								<input type="radio" id='subcatradio_1-3-3' value="1-4-3" name="subcatradio[4]">
								<span class="checkmark"></span>
							</label>
                        </td>
                    </tr>
                    <tr>
                        <td>Taste</td>
                        <td>
                            <label class="checkbox-box">
								<input type="radio" id='subcatradio_1-5-1' value="1-5-1" name="subcatradio[5]">
								<span class="checkmark"></span>
							</label>
                        </td>
                        <td>
                            <label class="checkbox-box">
								<input type="radio" id='subcatradio_1-5-2' value="1-5-2" name="subcatradio[5]">
								<span class="checkmark"></span>
							</label>
                        </td>
                        <td>
                            <label class="checkbox-box">
								<input type="radio" id='subcatradio_1-5-3' value="1-5-3" name="subcatradio[5]">
								<span class="checkmark"></span>
							</label>
                        </td>
                        <td>
                           <label class="checkbox-box">
								<input type="radio" id='subcatradio_1-5-4' value="1-5-4" name="subcatradio[5]">
								<span class="checkmark"></span>
							</label>
                        </td>
                    </tr>

                    </tbody>
                </table>
            </div>

        </div>
        <div class="tab-pane" id="tab3">
            <h2 class="page-header-title">STAFF</h2>
            <br>
            <br>
            <br>
            <div class="table-responsive">
                <table class="table mb-0">
                    <thead>
                    <tr>
                        <th>Item</th>
                        <th>Excellent</th>
                        <th>Good</th>
                        <th>Fair</th>
                        <th>Poor</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Appearance</td>
                        <td>
                            <label class="checkbox-box">
								<input type="radio" id='subcatradio_1-6-1' value="1-6-1" name="subcatradio[6]">
								<span class="checkmark"></span>
							</label>
                        </td>
                        <td>
                             <label class="checkbox-box">
								<input type="radio" id='subcatradio_1-6-2' value="1-6-2" name="subcatradio[6]">
								<span class="checkmark"></span>
							</label>
                        </td>
                        <td>
                            <label class="checkbox-box">
								<input type="radio" id='subcatradio_1-6-3' value="1-6-3" name="subcatradio[6]">
								<span class="checkmark"></span>
							</label>
                        </td>
                        <td>
                             <label class="checkbox-box">
								<input type="radio" id='subcatradio_1-6-4' value="1-6-4" name="subcatradio[6]">
								<span class="checkmark"></span>
							</label>
                        </td>
                    </tr>
                    <tr>
                        <td>Courtesy</td>
                        <td>
                            <label class="checkbox-box">
								<input type="radio" id='subcatradio_1-7-1' value="1-7-1" name="subcatradio[7]">
								<span class="checkmark"></span>
							</label>
                        </td>
                        <td>
                            <label class="checkbox-box">
								<input type="radio" id='subcatradio_1-7-2' value="1-7-2" name="subcatradio[7]">
								<span class="checkmark"></span>
							</label>
                        </td>
                        <td>
                             <label class="checkbox-box">
								<input type="radio" id='subcatradio_1-7-3' value="1-7-3" name="subcatradio[7]">
								<span class="checkmark"></span>
							</label>
                        </td>
                        <td>
                            <label class="checkbox-box">
								<input type="radio" id='subcatradio_1-7-4' value="1-7-4" name="subcatradio[7]">
								<span class="checkmark"></span>
							</label>
                        </td>
                    </tr>
                    <tr>
                        <td>Menu Knowledge
                        </td>
                        <td>
                            <label class="checkbox-box">
								<input type="radio" id='subcatradio_1-8-1' value="1-8-1" name="subcatradio[8]">
								<span class="checkmark"></span>
							</label>
                        </td>
                        <td>
                            <label class="checkbox-box">
								<input type="radio" id='subcatradio_1-8-2' value="1-8-2" name="subcatradio[8]">
								<span class="checkmark"></span>
							</label>
                        </td>
                        <td>
                             <label class="checkbox-box">
								<input type="radio" id='subcatradio_1-8-3' value="1-8-3" name="subcatradio[8]">
								<span class="checkmark"></span>
							</label>
                        </td>
                        <td>
                             <label class="checkbox-box">
								<input type="radio" id='subcatradio_1-8-4' value="1-8-4" name="subcatradio[8]">
								<span class="checkmark"></span>
							</label>
                        </td>
                    </tr>
                    <tr>
                        <td>Speed of Service</td>
                        <td>
                            <label class="checkbox-box">
								<input type="radio" id='subcatradio_1-9-1' value="1-9-1" name="subcatradio[9]">
								<span class="checkmark"></span>
							</label>
                        </td>
                        <td>
                             <label class="checkbox-box">
								<input type="radio" id='subcatradio_1-9-2' value="1-9-2" name="subcatradio[9]">
								<span class="checkmark"></span>
							</label>
                        </td>
                        <td>
                             <label class="checkbox-box">
								<input type="radio" id='subcatradio_1-9-3' value="1-9-3" name="subcatradio[9]">
								<span class="checkmark"></span>
							</label>
                        </td>
                        <td>
                            <label class="checkbox-box">
								<input type="radio" id='subcatradio_1-9-4' value="1-9-4" name="subcatradio[9]">
								<span class="checkmark"></span>
							</label>
                        </td>
                    </tr>

                    </tbody>
                </table>
            </div>

        </div>
        <div class="tab-pane" id="tab4">
		
            <h2 class="page-header-title">Outlet</h2>
            <br>
            <br>
            <br>

            <div class="table-responsive">
                <table class="table mb-0">
                    <thead>
                    <tr>

                        <th>Item</th>
                        <th>Excellent</th>
                        <th>Good</th>
                        <th>Fair</th>
                        <th>Poor</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Cleanliness</td>
                        <td>
                            <label class="checkbox-box">
								<input type="radio" id='subcatradio_1-10-1' value="1-10-1" name="subcatradio[10]">
								<span class="checkmark"></span>
							</label>
                        </td>
                        <td>
                             <label class="checkbox-box">
								<input type="radio" id='subcatradio_1-10-2' value="1-10-2" name="subcatradio[10]">
								<span class="checkmark"></span>
							</label>
                        </td>
                        <td>
                            <label class="checkbox-box">
								<input type="radio" id='subcatradio_1-10-3' value="1-10-3" name="subcatradio[10]">
								<span class="checkmark"></span>
							</label>
                        </td>
                        <td>
                            <label class="checkbox-box">
								<input type="radio" id='subcatradio_1-10-4' value="1-10-4" name="subcatradio[10]">
								<span class="checkmark"></span>
							</label>
                        </td>
                    </tr>
                    <tr>
                        <td>Menu Visibility
                        </td>
                        <td>
                             <label class="checkbox-box">
								<input type="radio" id='subcatradio_1-11-1' value="1-11-1" name="subcatradio[11]">
								<span class="checkmark"></span>
							</label>
                        </td>
                        <td>
                            <label class="checkbox-box">
								<input type="radio" id='subcatradio_1-11-2' value="1-11-2" name="subcatradio[11]">
								<span class="checkmark"></span>
							</label>
                        </td>
                        <td>
                            <label class="checkbox-box">
								<input type="radio" id='subcatradio_1-11-3' value="1-11-3" name="subcatradio[11]">
								<span class="checkmark"></span>
							</label>
                        </td>
                        <td>
                           <label class="checkbox-box">
								<input type="radio" id='subcatradio_1-11-4' value="1-11-4" name="subcatradio[11]">
								<span class="checkmark"></span>
							</label>
                        </td>
                    </tr>
                    <tr>
                        <td>Value for Money
                        </td>
                        <td>
                           <label class="checkbox-box">
								<input type="radio" id='subcatradio_1-12-1' value="1-12-1" name="subcatradio[12]">
								<span class="checkmark"></span>
							</label>
                        </td>
                        <td>
                            <label class="checkbox-box">
								<input type="radio" id='subcatradio_1-12-2' value="1-12-2" name="subcatradio[12]">
								<span class="checkmark"></span>
							</label>
                        </td>
                        <td>
                             <label class="checkbox-box">
								<input type="radio" id='subcatradio_1-12-3' value="1-12-3" name="subcatradio[12]">
								<span class="checkmark"></span>
							</label>
                        </td>
                        <td>
                             <label class="checkbox-box">
								<input type="radio" id='subcatradio_1-12-4' value="1-12-4" name="subcatradio[12]">
								<span class="checkmark"></span>
							</label>
                        </td>
                    </tr>
                    <tr>
                        <td>Overall Experience
                        </td>
                        <td>
                             <label class="checkbox-box">
								<input type="radio" id='subcatradio_1-13-1' value="1-13-1" name="subcatradio[13]">
								<span class="checkmark"></span>
							</label>
                        </td>
                        <td>
                            <label class="checkbox-box">
								<input type="radio" id='subcatradio_1-13-2' value="1-13-2" name="subcatradio[13]">
								<span class="checkmark"></span>
							</label>
                        </td>
                        <td>
                            <label class="checkbox-box">
								<input type="radio" id='subcatradio_1-13-3' value="1-13-3" name="subcatradio[13]">
								<span class="checkmark"></span>
							</label>
                        </td>
                        <td>
                            <label class="checkbox-box">
								<input type="radio" id='subcatradio_1-13-4' value="1-13-4" name="subcatradio[13]">
								<span class="checkmark"></span>
							</label>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        
		<div class="tab-pane" id="tab5">
			<h2 class="page-header-title">Over All Rating</h2>
			<br>
			<br>
			<br>


			<div class="row" style="padding-top: 30px">

			<div class="col-12">

			<p>Dear Customer, <br>Thank you for choosing Al-khan Rasturant Your feedback is valueable for us.</p>
			<div class='rating-stars text-center'>
			<ul id='stars' >
			<li class='star' title='Poor' data-value='1'>
			<i class='fa fa-star fa-fw'></i>
			</li>
			<li class='star' title='Fair' data-value='2'>
			<i class='fa fa-star fa-fw'></i>
			</li>
			<li class='star' title='Good' data-value='3'>
			<i class='fa fa-star fa-fw'></i>
			</li>
			<li class='star' title='Excellent' data-value='4'>
			<i class='fa fa-star fa-fw'></i>
			</li>
			<li class='star' title='WOW!!!' data-value='5'>
			<i class='fa fa-star fa-fw'></i>
			</li>
			</ul>
			</div>


			</div>
</form>
</div>
</div>
		
		
		
		
		
		
		<ul class="pager wizard text-center" style="margin-top: 25px">
            <li class="previous d-inline-block">
                <a href="javascript:;" class="btn btn-secondary ripple">Previous</a>
            </li>
            <li class="next d-inline-block">
                <a href="javascript:;" class="btn btn-gradient-01">Next</a>
            </li>
        </ul>
</div>
        </div>
    </div>
</div>

<script>
        $(document).ready(function(){
			
			
        $.noConflict();
        //var table = $('# your selector').DataTable();


            /* 1. Visualizing things on Hover - See next part for action on click */
            $('#stars li').on('mouseover', function(){
                var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

                // Now highlight all the stars that's not after the current hovered star
                $(this).parent().children('li.star').each(function(e){
                    if (e < onStar) {
                        $(this).addClass('hover');
                    }
                    else {
                        $(this).removeClass('hover');
                    }
                });

            }).on('mouseout', function(){
                $(this).parent().children('li.star').each(function(e){
                    $(this).removeClass('hover');
                });
            });


            /* 2. Action to perform on click */
            $('#stars li').on('click', function(){
                var onStar = parseInt($(this).data('value'), 10); // The star currently selected
                var stars = $(this).parent().children('li.star');

                for (i = 0; i < stars.length; i++) {
                    $(stars[i]).removeClass('selected');
                }

                for (i = 0; i < onStar; i++) {
                    $(stars[i]).addClass('selected');
                }

                // JUST RESPONSE (Not needed)
                var ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
                var msg = "";
                if (ratingValue > 1) {
                    msg = "Thanks! You rated this " + ratingValue + " stars.";
                }
                else {
                    msg = "We will improve ourselves. You rated this " + ratingValue + " stars.";
                }
                responseMessage(msg);

            });

            function responseMessage(msg) {
                $('.success-box').fadeIn(200);
                $('.success-box div.text-message').html("<span>" + msg + "</span>");
            }
			
			$('.from').datepicker({
					format: "dd-mm-yyyy",
					weekStart: 0,
					autoclose: true,
					todayHighlight: true
				});
				
				var $validator = $("#commentForm").validate({

                rules: {
                    name: {
                        required: true,
					

                    },

                    phone: {
                        required: true,
                    },
                },
				messages: {
                    name: {
                        required: "Please Enter Customer Name",
                    },
					phone: {
                        required: "Please Enter Customer Mob#",
                    },
                },
            });

            $('#rootwizard').bootstrapWizard({
                'tabClass': 'nav nav-pills',
                'onNext': function(tab, navigation, index) {
                    var $valid = $("#commentForm").valid();
                    if(!$valid) {
                        $validator.focusInvalid();
                        return false;
                    }
                }
            });
			
			

        });
		
		



    </script>