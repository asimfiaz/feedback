<?php

use app\controllers\ReviewtypeController;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Reviewtype */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="reviewtype-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-xs-4 nopad">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-xs-3 nopad">
            <?= $form->field($model, 'status', [
                'template' => '<div class="col-xs-12 nopad">{label}</div> <div class="col-xs-9">{input}{error}{hint}</div>',
            ])->radioList(array(1 => 'Active', 0 => 'InActive')); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4 nopad">
            <?= $form->field($model, 'emoji_id')
                ->dropDownList(
                    ReviewtypeController::getEmojis(),
                    ['id' => 'emojis']
                ) ?>
        </div>
        <div class="col-lg-4" id="preview"></div>
    </div>

    <div class="row">
        <div class="col-xs-4 nopad">
            <label class="col-lg-12" style=" height: 18px;"></label>
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>

<script>
    $(document).ready(function() {
        var selected = $('#emojis').val();

        $.ajax({
            url: "<?= Yii::$app->homeUrl ?>reviewtype/image",
            type: "GET",
            beforeSend: function() {
                $('#preview').html('<h4>Loading...</h4>');
            },
            data: {
                id: selected
            },
            cache: false,
            success: function(data) {
                $('#preview').html(
                    '<img src="<?= Yii::$app->homeUrl ?>frontend/' + data + '"  width="50px" />'
                );
            }
        });
    });

    $('#emojis').change(function() {
        var selected = $('#emojis').val();

        $.ajax({
            url: "<?= Yii::$app->homeUrl ?>reviewtype/image",
            type: "GET",
            beforeSend: function() {
                $('#preview').html('<h4>Loading...</h4>');
            },
            data: {
                id: selected
            },
            cache: false,
            success: function(data) {
                $('#preview').html('<img src="<?= Yii::$app->homeUrl ?>frontend/' + data + '"  width="50px" />');
            }
        });
    });
</script>