<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Reviewtype */

$this->title = Yii::t('app', 'Create Reviewtype');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Reviewtypes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reviewtype-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
